# Informações para desenvolvimento/manutenção/evolução do projeto
* Primeiramente deve ter o Maven e o JDK versão 11 instalados.

* Importe o projeto no eclipse
* C:\estudo\workspaceADS - Exemplo de diretoria, varia de utilizador para utilizador.

* Caso exista alguma alteração feita do lado do frontEnd (pasta ads-frontend), deve-se ir à diretoria dessa mesma pasta e executar:

* C:\estudo\workspaceADS\ads-frontend>npm install
* C:\estudo\workspaceADS\ads-frontend>npm run build

* Depois

* No terminal no diretorio onde se encontra o projeto (backend):
* C:\estudo\workspaceADS\sgia>mvnw clean install

* Depois sempre que precisar executar o projeto com qualquer alteração executar:
* C:\estudo\workspaceADS\sgia>mvnw spring-boot:run

* URLs para acesso:
* http://localhost:8080/sgia/

# Informações para executar o sistema via Docker
* Siga os passos seguintes, no seu terminal:
* • docker pull rafaelmachado5/ads_docker12:project

* • C:\estudo\workspaceADS\ > docker-compose -f docker-compose.yml up

* Serviço de avaliação via webservice a correr na porta 8083 e todo o projeto na porta 8080

* Aceder ao URL:
* • http://localhost:8080/sgia/
