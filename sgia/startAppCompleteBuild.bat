@echo off

cd ..\ads-frontend\
call npm install
call npm run build

cd ..\sgia\
call mvnw clean install
call mvnw spring-boot:run