package pt.iscte.ads.sgia.solution;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.uma.jmetal.solution.AbstractSolution;
import org.uma.jmetal.solution.permutationsolution.PermutationSolution;

@SuppressWarnings("serial")
public class JobShopSolution extends AbstractSolution<Integer> implements PermutationSolution<Integer> {

	// private IntegerPermutationSolution operationsPermutationSolution;
	private MachinePermutationSolution machinesPermutationSolution;

	/** Constructor */
	public JobShopSolution(int numberOfVariables, int numberOfObjectives) {
		super(numberOfVariables, numberOfObjectives);
		// operationsPermutationSolution = new
		// IntegerPermutationSolution(numberOfVariables, numberOfObjectives);
		List<Integer> randomSequence = new ArrayList<>(numberOfVariables);

		for (int j = 0; j < numberOfVariables; j++) {
			randomSequence.add(j);
		}

		java.util.Collections.shuffle(randomSequence);

		for (int i = 0; i < numberOfVariables; i++) {
			setVariable(i, randomSequence.get(i));
		}
		setMachinesPermutationSolution(new MachinePermutationSolution(numberOfVariables, numberOfObjectives, 2));
		// TODO Auto-generated constructor stub
	}

	/** Copy Constructor */
	public JobShopSolution(JobShopSolution solution) {
		// TODO Auto-generated constructor stub
		super(solution.getLength(), solution.getNumberOfObjectives());

		for (int i = 0; i < getNumberOfObjectives(); i++) {
			setObjective(i, solution.getObjective(i));
		}

		for (int i = 0; i < getNumberOfVariables(); i++) {
			setVariable(i, solution.getVariable(i));
		}

		for (int i = 0; i < getNumberOfConstraints(); i++) {
			setConstraint(i, solution.getConstraint(i));
		}

		attributes = new HashMap<Object, Object>(solution.attributes);
	}

	@Override
	public JobShopSolution copy() {
		return new JobShopSolution(this);
	}

	@Override
	public Map<Object, Object> getAttributes() {
		return attributes;
	}

	@Override
	public int getLength() {
		return getNumberOfVariables();
	}

	public MachinePermutationSolution getMachinesPermutationSolution() {
		return machinesPermutationSolution;
	}

	public void setMachinesPermutationSolution(MachinePermutationSolution machinesPermutationSolution) {
		this.machinesPermutationSolution = machinesPermutationSolution;
	}

}
