package pt.iscte.ads.sgia.simulator;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.util.CollectionUtils;

import pt.iscte.ads.sgia.enums.OperationState;
import pt.iscte.ads.sgia.model.Allocation;
import pt.iscte.ads.sgia.model.GenericSolution;
import pt.iscte.ads.sgia.model.Jobs1;
import pt.iscte.ads.sgia.model.Machine;
import pt.iscte.ads.sgia.model.Operation;
import pt.iscte.ads.sgia.model.OperationJobMachine;
import pt.iscte.ads.sgia.model.SchedulerResult;
import pt.iscte.ads.sgia.model.Task;
import pt.iscte.ads.sgia.model.TaskResult;

public class JobShopSimulator {

	private List<Task> tarefas;

	private List<OperationJobMachine> operations = new ArrayList<OperationJobMachine>();

	private long consumption = 0L;

	private long makespan;

	@SuppressWarnings("rawtypes")
	private GenericSolution genericSolution;

	public long simulateMakespan(List<Allocation> allocations, List<Jobs1> jobs, GenericSolution genericSolution,
			Map<Integer, List<Operation>> executionOrderOfOperations) {
		Date start = genericSolution.getTimeStart();
		long clock = start.getTime();

		// 1 - Obter todas as máquinas
		List<Machine> machines = getMachines(allocations);

		int counter = 0;

		Integer order = 1;
		while (!allJobsAreFinished(jobs)) {
			// 2 - Percorrer todas a máquinas, ver se estão disponíveis e alocar as
			// operações por ordem.
			schedule(allocations, genericSolution, clock, machines, order, executionOrderOfOperations, jobs);
			// 3 - Alterar o estado do sistema: Ver em todas as máquinas qual a operação que
			// demora menos tempo
			// e avançar o relógio tantas unidades de tempo quanto o tempo da operação que
			// possui um menor tempo de processamento.
			order++;
			clock = changeSystemState(clock, machines);

			if (counter > 10)
				System.out.println(counter);

			counter++;
		}

		System.out.println("Ordem de Execução: " + executionOrderOfOperations);

		// setGenericSolution(genericSolution);
		genericSolution.setTimeEnd(new Date(clock));
		genericSolution.setConsumption(consumption);
		return clock;
	}

	public long simulateConsumption(List<Allocation> allocations, List<Jobs1> jobs, GenericSolution genericSolution,
			Map<Integer, List<Operation>> executionOrderOfOperations) {
		Date start = genericSolution.getTimeStart();
		long clock = start.getTime();

		// 1 - Obter todas as máquinas
		List<Machine> machines = getMachines(allocations);

		Integer order = 1;
		while (!allJobsAreFinished(jobs)) {
			// 2 - Percorrer todas a máquinas, ver se estão disponíveis e alocar as
			// operações por ordem.
			schedule(allocations, genericSolution, clock, machines, order, executionOrderOfOperations, jobs);
			// 3 - Alterar o estado do sistema: Ver em todas as máquinas qual a operação que
			// demora menos tempo
			// e avançar o relógio tantas unidades de tempo quanto o tempo da operação que
			// possui um menor tempo de processamento.
			order++;
			clock = changeSystemState(clock, machines);
		}

		genericSolution.setTimeEnd(new Date(clock));
		genericSolution.setConsumption(consumption);
		return consumption;
	}

	private List<Machine> getMachines(List<Allocation> allocations) {
		List<Machine> machines = new ArrayList<Machine>();
		for (Allocation allocation : allocations) {
			Machine machine = allocation.getMachine();
			if (!machines.contains(machine))
				machines.add(allocation.getMachine());
		}
		return machines;
	}

	private long changeSystemState(long clock, List<Machine> machines) {
		long shortestTime = Long.MAX_VALUE;
		int counter = 0;
		int index = 0;
		List<Integer> equalsMachines = new ArrayList<Integer>();
		System.out.println("Alterar estado do sistema: ");
		System.out.println("Clock inicial: " + new Date(clock));
		for (Machine machine : machines) {
			counter++;
			if (machine.getAssignedOperation() != null && machine.getAssignedOperation().isFinished == false) {
				Operation operation = machine.getAssignedOperation();
				long processingTime = operation.endTime - clock;
				if (processingTime > Long.MAX_VALUE)
					System.out.println(new Date(processingTime));
				if (processingTime <= shortestTime && operation.isExecuting && processingTime > 0
						&& !machine.isAvailable()) {
					if(processingTime == shortestTime)
						equalsMachines.add(counter);
					else
						index = counter;
					//index
					shortestTime = processingTime;
					System.out.println("Index ficou a: " + index);
					// operation.isExecuting = false;
					// operation.isFinished = true;
					// operation.getJob().isExecuting = false;
					// machine.setAvailable(true);
				}
			}
//			if (index != 0) {
//				machines.get(index - 1).getAssignedOperation().isExecuting = false;
//				machines.get(index - 1).getAssignedOperation().isFinished = true;
//				machines.get(index - 1).getAssignedOperation().getJob().isExecuting = false;
//				machines.get(index - 1).setAvailable(true);
//			}
		}

		if (index != 0) {
			machines.get(index - 1).getAssignedOperation().isExecuting = false;
			machines.get(index - 1).getAssignedOperation().isFinished = true;
			machines.get(index - 1).getAssignedOperation().getJob().isExecuting = false;
			machines.get(index - 1).setAvailable(true);
			System.out.println(machines.get(index - 1) + " disponível!");
			if(!equalsMachines.isEmpty()) {
				for (Integer integer : equalsMachines) {
					if (integer > index) {
						machines.get(integer - 1).getAssignedOperation().isExecuting = false;
						machines.get(integer - 1).getAssignedOperation().isFinished = true;
						machines.get(integer - 1).getAssignedOperation().getJob().isExecuting = false;
						machines.get(integer - 1).setAvailable(true);
					}
				}
			}
		}

		if (shortestTime > 0 && shortestTime < Long.MAX_VALUE) {
			clock += shortestTime;
		}

		System.out.println("Clock final: " + new Date(clock));

		return clock;
	}

	private void schedule(List<Allocation> allocations, GenericSolution genericSolution, long clock,
			List<Machine> machines, Integer order, Map<Integer, List<Operation>> executionOrderOfOperations,
			List<Jobs1> jobs) {
		boolean flag = false;
		List<Operation> orderedOperations = new ArrayList<Operation>();
		for (Machine machine : machines) {
			if (machine.isAvailable()) {
				System.out.println("Máquina " + machine.getName() + " encontra-se livre");
				for (Allocation allocation : allocations) {
					Operation operation = allocation.getOperation();
					Machine allocatedMachine = allocation.getMachine();
					if (!operation.isFinished && !operation.isExecuting && !operation.getJob().isExecuting
							&& allocatedMachine.getName() == machine.getName()) {
						flag = true;
						if (machine.getTimeLastRun() == null)
							machine.setTimeLastRun(genericSolution.getTimeStart());
						machine.setAvailable(false);
						System.out.println(machine.getName() + " ocupada!");
						machine.setAssignedOperation(operation);
						operation.isExecuting = true;
						operation.getJob().isExecuting = true;

						System.out.println(
								"A Máquina " + machine.getName() + " será ocupada pelas seguintes operações: ");

						operation.startTime = clock;
						// operation.startTime = machine.getTimeLastRun().getTime();
						// operation.endTime = clock + (long) (allocation.getProcessingTime() * 1000);
						operation.endTime = operation.startTime + (long) (allocation.getProcessingTime() * 1000);

						consumption += (long) ((long) operation.value
								* (machine.getValue() / (allocation.getProcessingTime() * 1000)));

						OperationJobMachine operationJobMachine = new OperationJobMachine();
						operationJobMachine.setJob(operation.getJob().getName());
						operationJobMachine.setOperation(operation.getName());
						operationJobMachine.setValue(operation.getValue());

						Task task = new Task(operationJobMachine);
						task.getOperationJobMachine().setMachine(machine.getName());
						// task.setOperationState(OperationState.RUNNING);

						Calendar calendarStartDate = Calendar.getInstance();
						Calendar calendarEndDate = Calendar.getInstance();
						calendarStartDate.setTimeInMillis(operation.startTime);
						calendarEndDate.setTimeInMillis(operation.endTime);
						Date startDate = calendarStartDate.getTime();
						Date endDate = calendarEndDate.getTime();

						machine.setTimeLastRun(endDate);

						task.setBeginOperation(startDate);
						task.setEndOperation(endDate);

						// genericSolution.setTimeEnd(endDate);

						operation.setStartDate(startDate);
						operation.setEndDate(endDate);

						System.out.println("A operação " + operation + " irá executar nesta máquina das " + startDate
								+ " às " + endDate);

						parseTaskResult(genericSolution, task, startDate, endDate);

						orderedOperations.add(operation);

						break;
					}
				}
			}
		}
		if (flag) {
			executionOrderOfOperations.put(order, orderedOperations);
		}
	}

	public static boolean allJobsAreFinished(List<Jobs1> jobs) {
		for (Jobs1 job : jobs) {
			List<Operation> operations = job.getOperations();
			for (Operation operation : operations) {
				if (!operation.isFinished)
					return false;
			}
		}
		return true;
	}

	private static void parseTaskResult(GenericSolution genericSolution, Task task, Date timeProcess, Date endDate) {

		boolean isNewMachine = true;

		if (!CollectionUtils.isEmpty(genericSolution.getListSchedulerResult())) {
			for (int i = 0; i < genericSolution.getListSchedulerResult().size(); i++) {

				SchedulerResult schedulerResult = (SchedulerResult) genericSolution.getListSchedulerResult().get(i);

				if (schedulerResult != null
						&& task.getOperationJobMachine().getMachine().equals(schedulerResult.getMachine())) {

					TaskResult taskResult = new TaskResult();
					taskResult.setJob(task.getOperationJobMachine().getJob());
					taskResult.setOperation(task.getOperationJobMachine().getOperation());
					taskResult.setBeginOperation(timeProcess);
					taskResult.setEndOperation(endDate);

					schedulerResult.getListTaskResult().add(taskResult);

					isNewMachine = false;
				}
			}
		}

		if (isNewMachine) {

			SchedulerResult schedulerResult = new SchedulerResult();

			schedulerResult.setMachine(task.getOperationJobMachine().getMachine());
			schedulerResult.setListTaskResult(new ArrayList<TaskResult>());

			TaskResult taskResult = new TaskResult();
			taskResult.setJob(task.getOperationJobMachine().getJob());
			taskResult.setOperation(task.getOperationJobMachine().getOperation());
			taskResult.setBeginOperation(timeProcess);
			taskResult.setEndOperation(endDate);

			schedulerResult.getListTaskResult().add(taskResult);

			if (CollectionUtils.isEmpty(genericSolution.getListSchedulerResult())) {
				genericSolution.setListSchedulerResult(new ArrayList<SchedulerResult>());
			}

			genericSolution.getListSchedulerResult().add(schedulerResult);
		}
	}

	public GenericSolution getGenericSolution() {
		return genericSolution;
	}

	public void setGenericSolution(GenericSolution genericSolution) {
		this.genericSolution = genericSolution;
	}

}
