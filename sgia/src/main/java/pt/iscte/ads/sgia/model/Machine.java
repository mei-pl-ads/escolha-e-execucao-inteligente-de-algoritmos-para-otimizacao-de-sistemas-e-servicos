package pt.iscte.ads.sgia.model;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Machine {

	private int machineId;

	private String name;

	private Integer value;

	private int priority;

	private String dueto;

	private boolean available = true;

	private double executionDate;

	private Operation assignedOperation;

	private int machinesNumber;

	private Map<String, Object> additionalProperties = new HashMap<String, Object>();
	
	private boolean firstRun = true;
	
	private Date timeStartRun;
	
	private Date timeLastRun;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getValue() {
		return value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}

//	public Boolean getIsAvailable() {
//		return isAvailable;
//	}
//
//	public void setIsAvailable(Boolean isAvailable) {
//		this.isAvailable = isAvailable;
//	}

	public double getExecutionDate() {
		return executionDate;
	}

	public void setExecutionDate(double executionDate) {
		this.executionDate = executionDate;
	}

	public Map<String, Object> getAdditionalProperties() {
		return additionalProperties;
	}

	public void setAdditionalProperties(Map<String, Object> additionalProperties) {
		this.additionalProperties = additionalProperties;
	}

	public int getMachineId() {
		int digitsCount = 0;
		for (int i = 0; i < name.length(); i++) {
			if (Character.isDigit(name.charAt(i))) {
				digitsCount++;
			}
		}
		return Integer.valueOf(String.valueOf(name.substring(name.length() - digitsCount))) - 1;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public String getDueto() {
		return dueto;
	}

	public void setDueto(String dueto) {
		this.dueto = dueto;
	}

	public Operation getAssignedOperation() {
		return assignedOperation;
	}

	public void setAssignedOperation(Operation assignedOperation) {
		this.assignedOperation = assignedOperation;
	}

	public int getMachinesNumber() {
		return machinesNumber;
	}

	public void setMachinesNumber(int machinesNumber) {
		this.machinesNumber = machinesNumber;
	}

	public Machine getNewMachine(Machine machine) {
		Machine mach = new Machine();
		mach.machineId = machine.machineId;
		mach.name = machine.name;
		mach.priority = machine.priority;
		mach.dueto = machine.dueto;
		mach.available = machine.available;
		mach.executionDate = machine.executionDate;
		mach.assignedOperation = machine.assignedOperation;
		mach.machinesNumber = machine.machinesNumber;
		mach.value = machine.value;
		return mach;
	}

	public boolean isAvailable() {
		return available;
	}

	public void setAvailable(boolean available) {
		this.available = available;
	}

	public boolean isFirstRun() {
		return firstRun;
	}

	public void setFirstRun(boolean firstRun) {
		this.firstRun = firstRun;
	}

	@Override
	public String toString() {
		return getName();
	}	
	
	public Date getTimeStartRun() {
		return timeStartRun;
	}

	public void setTimeStartRun(Date timeStartRun) {
		this.timeStartRun = timeStartRun;
	}

	public Date getTimeLastRun() {
		return timeLastRun;
	}

	public void setTimeLastRun(Date timeLastRun) {
		this.timeLastRun = timeLastRun;
	}
}