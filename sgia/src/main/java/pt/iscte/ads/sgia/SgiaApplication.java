package pt.iscte.ads.sgia;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SgiaApplication {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(SgiaApplication.class);

	public static void main(String[] args) {
		LOGGER.info("-> Iniciando a aplicação SGIA - Sistema de Gestão Inteligente de Algoritmos");
		SpringApplication.run(SgiaApplication.class, args);
		LOGGER.info("-> Aplicação SGIA - Sistema de Gestão Inteligente de Algoritmos iniciada!");
	
	}

}
