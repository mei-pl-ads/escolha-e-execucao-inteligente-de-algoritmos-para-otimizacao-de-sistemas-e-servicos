package pt.iscte.ads.sgia.factory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;
import org.uma.jmetal.problem.Problem;
import org.uma.jmetal.solution.permutationsolution.PermutationSolution;

import pt.iscte.ads.sgia.model.AlgorithmRequest;
import pt.iscte.ads.sgia.model.GenericSolution;
import pt.iscte.ads.sgia.problem.MakespanFlowShopProblem;
import pt.iscte.ads.sgia.problem.MakspanJobShopProblem;
import pt.iscte.ads.sgia.problem.SimpleJobShopProblem;

@Component
public class SolutionResultFactory {

	@SuppressWarnings("rawtypes")
	public <T, S> List<GenericSolution> getSolutionResult(AlgorithmRequest algorithmRequest, Problem<T> problem,
			List<S> result) {

		List<GenericSolution> genericSolutionResult = null;

		if (isMakspanJobShopProblem(problem)) {
			genericSolutionResult = parserMakspanJobShopProblem(algorithmRequest, problem, result);
		} else if (isJobShopProblem(problem)) {
			genericSolutionResult = parserJobShopProblem(algorithmRequest, problem, result);
		} else if (isMakspanFlowShopProblem(problem)) {
			genericSolutionResult = parserMakespanFlowShopProblem(algorithmRequest, problem, result);
		}

		return genericSolutionResult;
	}

	public <T> boolean isMakspanJobShopProblem(Problem<T> problem) {
		return problem instanceof MakspanJobShopProblem;
	}

	public <T> boolean isJobShopProblem(Problem<T> problem) {
		return problem instanceof SimpleJobShopProblem;
	}

	public <T> boolean isMakspanFlowShopProblem(Problem<T> problem) {
		return problem instanceof MakespanFlowShopProblem;
	}

	@SuppressWarnings("rawtypes")
	private <S, T> List<GenericSolution> parserMakspanJobShopProblem(AlgorithmRequest algorithmRequest,
			Problem<T> problem, List<S> result) {

		MakspanJobShopProblem makspanJobShopProblem = (MakspanJobShopProblem) problem;

		@SuppressWarnings("unchecked")
		List<PermutationSolution<Integer>> solution = (List<PermutationSolution<Integer>>) result;

		List<GenericSolution> listGenericSolution = new ArrayList<GenericSolution>();

		for (PermutationSolution<Integer> permutationSolution : solution) {

			@SuppressWarnings("unchecked")
			GenericSolution<T> genericSolution = findGenericSolution(makspanJobShopProblem.getListaGenericSolution(),
					permutationSolution.getVariables().toString());

			if (genericSolution != null && !isIncluded(listGenericSolution, genericSolution)) {
				listGenericSolution.add(genericSolution);
			}
		}

		return listGenericSolution;
	}

	@SuppressWarnings("rawtypes")
	private <S, T> List<GenericSolution> parserJobShopProblem(AlgorithmRequest algorithmRequest, Problem<T> problem,
			List<S> result) {

		SimpleJobShopProblem simpleJobShopProblem = (SimpleJobShopProblem) problem;

		@SuppressWarnings("unchecked")
		List<PermutationSolution<Integer>> solution = (List<PermutationSolution<Integer>>) result;

		List<GenericSolution> listGenericSolution = new ArrayList<GenericSolution>();

		for (PermutationSolution<Integer> permutationSolution : solution) {

			@SuppressWarnings("unchecked")
			GenericSolution<T> genericSolution = findResultSolution(simpleJobShopProblem.getSolutionsMap(),
					permutationSolution);

			if (genericSolution != null && !isIncluded(listGenericSolution, genericSolution)) {
				listGenericSolution.add(genericSolution);
			}
		}

		return listGenericSolution;
	}
	
	@SuppressWarnings("rawtypes")
	private <S, T> List<GenericSolution> parserMakespanFlowShopProblem(AlgorithmRequest algorithmRequest,
			Problem<T> problem, List<S> result) {

		MakespanFlowShopProblem makespanFlowShopProblem = (MakespanFlowShopProblem) problem;

		@SuppressWarnings("unchecked")
		List<PermutationSolution<Integer>> solution = (List<PermutationSolution<Integer>>) result;

		List<GenericSolution> listGenericSolution = new ArrayList<GenericSolution>();

		for (PermutationSolution<Integer> permutationSolution : solution) {

			@SuppressWarnings("unchecked")
			GenericSolution<T> genericSolution = findGenericSolution(makespanFlowShopProblem.getListaGenericSolution(),
					permutationSolution.getVariables().toString());

			if (genericSolution != null && !isIncluded(listGenericSolution, genericSolution)) {
				listGenericSolution.add(genericSolution);
			}
		}

		return listGenericSolution;
	}

	private <T> GenericSolution findResultSolution(Map<Integer, GenericSolution> solutionsMap,
			PermutationSolution<Integer> permutationSolution) {
		return solutionsMap.get(permutationSolution.hashCode());
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private <T> GenericSolution findGenericSolution(List<GenericSolution> listaGenericSolution, String idVariables) {
		for (GenericSolution<T> genericSolution : listaGenericSolution) {
			if (genericSolution.getCodIdentify().equals(idVariables)) {
				return genericSolution;
			}
		}
		return null;
	}

	private <T> boolean isIncluded(@SuppressWarnings("rawtypes") List<GenericSolution> listGenericSolution,
			GenericSolution<T> genericSolution) {
		boolean isIncluded = false;
		for (@SuppressWarnings("rawtypes")
		GenericSolution genericSolution1 : listGenericSolution) {
			if (genericSolution1.getCodIdentify().equals(genericSolution.getCodIdentify())) {
				isIncluded = true;
				break;
			}
		}
		return isIncluded;
	}
}

