package pt.iscte.ads.sgia.model;

import java.util.Date;
import java.util.List;

import pt.iscte.ads.sgia.enums.OperationState;

public class Operation {

	private int id;

	private OperationState operationState = OperationState.NOT_STARTED;

	public String name;

	public List<String> machine;

	public int order;

	public String preemptive;

	public int value;

	public String after;

	public long startTime;

	public long endTime;

	public Date startDate;

	public Date endDate;

	public boolean isExecuting = false;

	public boolean isFinished = false;

	private Machine assignedMachine;

	private Jobs1 job;
	
	private Operation afterOperation;

	public Operation getAfterOperation() {
		return afterOperation;
	}

	public void setAfterOperation(Operation afterOperation) {
		this.afterOperation = afterOperation;
	}

	public int getId() {
		return Integer.valueOf(String.valueOf(name.charAt(name.length() - 1))) - 1;
	}

	public void setId(int id) {
		this.id = id;
	}

	public OperationState getOperationState() {
		return operationState;
	}

	public void setOperationState(OperationState operationState) {
		this.operationState = operationState;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<String> getMachine() {
		return machine;
	}

	public void setMachine(List<String> machine) {
		this.machine = machine;
	}

	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	public String getPreemptive() {
		return preemptive;
	}

	public void setPreemptive(String preemptive) {
		this.preemptive = preemptive;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public String getAfter() {
		return after;
	}

	public void setAfter(String after) {
		this.after = after;
	}

	public Machine getAssignedMachine() {
		return assignedMachine;
	}

	public void setAssignedMachine(Machine assignedMachine) {
		this.assignedMachine = assignedMachine;
	}

	public Jobs1 getJob() {
		return job;
	}

	public void setJob(Jobs1 job) {
		this.job = job;
	}

	public Date getStartDate() {
		return startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	@Override
	public String toString() {
		return job.getName() + " - " + getName();
	}

}