package pt.iscte.ads.sgia.model;

import java.util.Date;

import pt.iscte.ads.sgia.enums.OperationState;

public class Task {

	private OperationJobMachine operationJobMachine;
	
	private OperationState operationState = OperationState.NOT_STARTED;
	
	private Date beginOperation;
	
	private Date endOperation;
	
	public Task(OperationJobMachine operationJobMachine) {
		this.operationJobMachine = operationJobMachine;
	}

	public OperationJobMachine getOperationJobMachine() {
		return operationJobMachine;
	}

	public void setOperationJobMachine(OperationJobMachine operationJobMachine) {
		this.operationJobMachine = operationJobMachine;
	}

	public OperationState getOperationState() {
		return operationState;
	}

	public void setOperationState(OperationState operationState) {
		this.operationState = operationState;
	}

	public Date getBeginOperation() {
		return beginOperation;
	}

	public void setBeginOperation(Date beginOperation) {
		this.beginOperation = beginOperation;
	}

	public Date getEndOperation() {
		return endOperation;
	}

	public void setEndOperation(Date endOperation) {
		this.endOperation = endOperation;
	}

}
