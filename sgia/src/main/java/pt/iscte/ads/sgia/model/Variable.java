package pt.iscte.ads.sgia.model;

public class Variable {

	private String from;
	private Integer index;
	private String name;
	private String to;

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public Integer getIndex() {
		return index;
	}

	public void setIndex(Integer index) {
		this.index = index;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	@Override
	public String toString() {
		return "Variable [from=" + from + ", index=" + index + ", name=" + name + ", to=" + to + "]";
	}

}
