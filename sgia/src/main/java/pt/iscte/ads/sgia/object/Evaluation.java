package pt.iscte.ads.sgia.object;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Evaluation {

	private String solution;
	private Boolean evaluation;
	private Double evaluationValue;

	public String getSolution() {
		return solution;
	}

	public void setSolution(String solution) {
		this.solution = solution;
	}

	public Boolean getEvaluation() {
		return evaluation;
	}

	public Double getEvaluationValue() {
		return evaluationValue;
	}

	public void setEvaluation(Boolean evaluation) {
		this.evaluation = evaluation;
	}

	public void setEvaluationValue(Double evaluationValue) {
		this.evaluationValue = evaluationValue;
	}

	@Override
	public String toString() {
		return "Evaluation{" + "type='" + evaluation + '\'' + ", value=" + evaluationValue + '}';
	}

}
