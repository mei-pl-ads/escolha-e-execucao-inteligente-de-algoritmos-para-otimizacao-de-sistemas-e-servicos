package pt.iscte.ads.sgia.model;

import java.util.List;

public class AlgorithmRequest {

	private String problemId;

	private String problemType;

	private String problemName;

	private String machinesEqual;

	private List<Machine> machines = null;

	private String isAllMachinesSame;

	private List<Job> jobs = null;

	private List<Jobs1> jobs1 = null;

	private List<Objective> objectives = null;

	private List<String> heuristic = null;

	private List<String> startTime = null;

	private Parameters aParams;

	public String getProblemId() {
		return problemId;
	}

	public void setProblemId(String problemId) {
		this.problemId = problemId;
	}

	public String getProblemType() {
		return problemType;
	}

	public void setProblemType(String problemType) {
		this.problemType = problemType;
	}

	public String getProblemName() {
		return problemName;
	}

	public void setProblemName(String problemName) {
		this.problemName = problemName;
	}

	public String getMachinesEqual() {
		return machinesEqual;
	}

	public void setMachinesEqual(String machinesEqual) {
		this.machinesEqual = machinesEqual;
	}

	public List<Machine> getMachines() {
		return machines;
	}

	public void setMachines(List<Machine> machines) {
		this.machines = machines;
	}

	public String getIsAllMachinesSame() {
		return isAllMachinesSame;
	}

	public void setIsAllMachinesSame(String isAllMachinesSame) {
		this.isAllMachinesSame = isAllMachinesSame;
	}

	public List<Job> getJobs() {
		return jobs;
	}

	public void setJobs(List<Job> jobs) {
		this.jobs = jobs;
	}

	public List<Jobs1> getJobs1() {
		return jobs1;
	}

	public void setJobs1(List<Jobs1> jobs1) {
		this.jobs1 = jobs1;
	}

	public List<Objective> getObjectives() {
		return objectives;
	}

	public void setObjectives(List<Objective> objectives) {
		this.objectives = objectives;
	}

	public List<String> getStartTime() {
		return startTime;
	}

	public void setStartTime(List<String> startTime) {
		this.startTime = startTime;
	}

	public Parameters getaParams() {
		return aParams;
	}

	public void setaParams(Parameters aParams) {
		this.aParams = aParams;
	}

	public List<String> getHeuristic() {
		return heuristic;
	}

	public void setHeuristic(List<String> heuristic) {
		this.heuristic = heuristic;
	}

	@Override
	public String toString() {
		return "AlgorithmRequest [problemId=" + problemId + ",problemType=" + problemType + ", problemName="
				+ problemName + ", machinesEqual=" + machinesEqual + ", machines=" + machines + ", isAllMachinesSame="
				+ isAllMachinesSame + ", jobs=" + jobs + ", objectives=" + objectives + ", startTime=" + startTime
				+ "]";
	}

}
