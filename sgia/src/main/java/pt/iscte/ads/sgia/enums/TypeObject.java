package pt.iscte.ads.sgia.enums;

public enum TypeObject {

	BOOLEAN("Boolean"), 
	DOUBLE("Double"), 
	INTEGER("Integer");

	private String description;

	private TypeObject(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public static TypeObject getTypeObjectByDescription(String description)  {
		if (BOOLEAN.getDescription().equalsIgnoreCase(description)) {
			return TypeObject.BOOLEAN;
		} else if (DOUBLE.getDescription().equalsIgnoreCase(description)) {
			return TypeObject.DOUBLE;
		} else if (INTEGER.getDescription().equalsIgnoreCase(description)) {
			return TypeObject.INTEGER;
		}
		
		return null;
	}

}
