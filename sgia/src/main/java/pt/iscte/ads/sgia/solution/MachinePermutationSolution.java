package pt.iscte.ads.sgia.solution;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

import org.uma.jmetal.solution.AbstractSolution;
import org.uma.jmetal.solution.permutationsolution.PermutationSolution;

@SuppressWarnings("serial")
public class MachinePermutationSolution extends AbstractSolution<Integer> implements PermutationSolution<Integer> {

	/** Constructor */
	public MachinePermutationSolution(int permutationLength, int numberOfObjectives, int numberOfMachines) {
		super(permutationLength, numberOfObjectives);

		List<Integer> randomSequence = new ArrayList<>(permutationLength);

		for (int j = 0; j < permutationLength; j++) {
			int randomNum = ThreadLocalRandom.current().nextInt(0, numberOfMachines);
			randomSequence.add(randomNum);
		}

		java.util.Collections.shuffle(randomSequence);

		for (int i = 0; i < permutationLength; i++) {
			setVariable(i, randomSequence.get(i));
		}
	}

	/** Copy Constructor */
	public MachinePermutationSolution(MachinePermutationSolution solution) {
		super(solution.getLength(), solution.getNumberOfObjectives());

		for (int i = 0; i < getNumberOfObjectives(); i++) {
			setObjective(i, solution.getObjective(i));
		}

		for (int i = 0; i < getNumberOfVariables(); i++) {
			setVariable(i, solution.getVariable(i));
		}

		for (int i = 0; i < getNumberOfConstraints(); i++) {
			setConstraint(i, solution.getConstraint(i));
		}

		attributes = new HashMap<Object, Object>(solution.attributes);
	}

	@Override
	public MachinePermutationSolution copy() {
		return new MachinePermutationSolution(this);
	}

	@Override
	public Map<Object, Object> getAttributes() {
		return attributes;
	}

	@Override
	public int getLength() {
		return getNumberOfVariables();
	}
}