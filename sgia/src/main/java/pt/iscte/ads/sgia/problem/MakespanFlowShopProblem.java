package pt.iscte.ads.sgia.problem;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.util.CollectionUtils;
import org.uma.jmetal.problem.permutationproblem.impl.AbstractIntegerPermutationProblem;
import org.uma.jmetal.solution.permutationsolution.PermutationSolution;

import pt.iscte.ads.sgia.enums.OperationState;
import pt.iscte.ads.sgia.model.AlgorithmRequest;
import pt.iscte.ads.sgia.model.GenericSolution;
import pt.iscte.ads.sgia.model.Job;
import pt.iscte.ads.sgia.model.Jobs1;
import pt.iscte.ads.sgia.model.Machine;
import pt.iscte.ads.sgia.model.Operation;
import pt.iscte.ads.sgia.model.OperationJobMachine;
import pt.iscte.ads.sgia.model.SchedulerResult;
import pt.iscte.ads.sgia.model.Task;
import pt.iscte.ads.sgia.model.TaskResult;
import pt.iscte.ads.sgia.util.DateUtil;
import pt.iscte.ads.sgia.util.MachineUtil;

public class MakespanFlowShopProblem extends AbstractIntegerPermutationProblem {

	private static final long serialVersionUID = -594974920330571204L;
	
	private List<Machine> machines;
	
	private List<Job> job;
	
	private List<Jobs1> jobs1;
	
	private List<OperationJobMachine> operations = new ArrayList<OperationJobMachine>();

	private PermutationSolution<Integer> betterSolution;
	
	private long betterTime;
	
	private List<GenericSolution> listaGenericSolution = new ArrayList<GenericSolution>();
	
	private Date startTime;

	private Map<Integer, String> jobsOperacao;
	
	public MakespanFlowShopProblem(AlgorithmRequest algorithmRequest) {
		
		setJob(algorithmRequest.getJobs());
		
		setMachines(algorithmRequest.getMachines());
		
		setJobs1(algorithmRequest.getJobs1());
		
		setStartTime(DateUtil.convertStartTime(algorithmRequest.getStartTime()));
		
		jobsOperacao = new HashMap<Integer, String>();
		
		int countOperation = 0;
		for (Jobs1 job : jobs1) {
			for (Operation operation : job.getOperations()) {
				String value = job.getName() + "," + operation.getName();
				jobsOperacao.put(countOperation++, value);
			}
		}

		setNumberOfVariables(jobsOperacao.size());
	    setNumberOfObjectives(1);
	    setName("MakespanFlowShopProblem");
	}

	@Override
	public int getLength() {
		return jobsOperacao.size();
	}

	@Override
	public void evaluate(PermutationSolution<Integer> solution) {
		System.out.println("------------------------ NEW SOLUTION: " + solution.getVariables());
		
		GenericSolution genericSolution = new GenericSolution();
		genericSolution.setTimeStart(startTime);
		
		genericSolution.setCodIdentify(solution.getVariables().toString());
		genericSolution.setVariables(solution.getVariables());
		
		List<Task> orderSimulatorBackup = new ArrayList<Task>();

		List<Task> orderSimulator = new ArrayList<Task>();
		
		for (int variable : solution.getVariables()) {
			
			String nameJobOperation = jobsOperacao.get(variable);
			
			String [] arrayNameJobOperation = nameJobOperation.split(",");
			
			String nameJob = arrayNameJobOperation[0];
			String nameOperations = arrayNameJobOperation[1];
			
			for (Jobs1 job : jobs1) {
				
				if (job.getName().equals(nameJob)) {
					for (Operation operation : job.getOperations()) {
						
						if (operation.getName().equals(nameOperations)) {
							
							if (!CollectionUtils.isEmpty(operation.getMachine())) {
								for (String nameMachines : operation.getMachine()) {
									
									OperationJobMachine operationJobMachine = new OperationJobMachine();
									
									operationJobMachine.setJob(job.getName());
									operationJobMachine.setOperation(operation.getName());
									operationJobMachine.setValue(operation.getValue());					
									operationJobMachine.setMachine(nameMachines);
									
									operations.add(operationJobMachine);
									
									Task task = new Task(operationJobMachine);
									orderSimulator.add(task);
								}
							}
						}
					}
				}
			}
		}
		
		orderSimulatorBackup.addAll(orderSimulator);
		
		Date timeProcess = startTime;
		
		long start = timeProcess.getTime();
		System.out.println("###### INICIO start: " + start + ", time: " + DateUtil.dateFormat.format(timeProcess));
		
		for (int i = 0; i < machines.size(); i++) {
			Machine machine = machines.get(i);
			machine.setFirstRun(true);
			if (i > 0) {
				machine.setAvailable(false);
			}
		}
		
		boolean firstRun = true;
		boolean isFinish = false;

		while (isAnyOperationPending(orderSimulator)) {
			
			for (Task task : orderSimulator) {

				Machine machine = MachineUtil.findMachinByName(machines, task.getOperationJobMachine().getMachine());
				
				Machine beforeMachine = MachineUtil.findBeforeMachine(machines, machine.getName());

				Task beforeTask = null;
				if (beforeMachine != null) {
					beforeTask = findBeforeTask(orderSimulatorBackup, beforeMachine.getName(), task.getOperationJobMachine().getJob(), task.getOperationJobMachine().getOperation());
				}
				
				if (OperationState.NOT_STARTED.equals(task.getOperationState()) && machine.isAvailable() 
						&& (beforeTask == null || OperationState.SUCCESS.equals(beforeTask.getOperationState()))) {
					
					System.out.println("###### MAQUINA: " + machine.getName() + ", ULTIMA EXECUÇÃO: " + (machine.getTimeLastRun()));
					System.out.println("###### OPERACAO: " + task.getOperationJobMachine().getOperation());

					if (machine.isFirstRun()) {
						if (beforeMachine != null && beforeMachine.getTimeLastRun() != null) {
							task.setBeginOperation(beforeMachine.getTimeLastRun());
						} else {
							task.setBeginOperation(startTime);
						}
					} else if (beforeTask != null && machine.getTimeLastRun() != null) {
						if (beforeTask.getEndOperation().compareTo(machine.getTimeLastRun()) < 0) {
							task.setBeginOperation(machine.getTimeLastRun());
						} else {
							task.setBeginOperation(beforeTask.getEndOperation());
						}
						
					} else if (machine.getTimeLastRun() != null) {
						task.setBeginOperation(machine.getTimeLastRun());
					}

					machine.setTimeStartRun(task.getBeginOperation());
					machine.setTimeLastRun(null);
					
					long timeSimulator = timeSimulatorMillisecond(task.getOperationJobMachine().getValue(), machine.getValue());

					Calendar calendarEndDate = Calendar.getInstance();
					calendarEndDate.setTimeInMillis(task.getBeginOperation().getTime() + timeSimulator);
				    Date endDate = calendarEndDate.getTime();
					task.setEndOperation(endDate);
					
					parseTaskResult(genericSolution, task, task.getBeginOperation(), endDate);

					if (machine.isFirstRun()) {
						timeProcess = endDate;
						machine.setFirstRun(false);
					}
					
					task.setOperationState(OperationState.RUNNING);
					machine.setAvailable(false);

				}
			}

			Iterator<Task> iteratorSimulator = orderSimulator.iterator();
			while (iteratorSimulator.hasNext()) {
				Task task = iteratorSimulator.next(); 
				
				if (OperationState.RUNNING.equals(task.getOperationState()) && isFinish)  {
					timeProcess = task.getEndOperation();
					isFinish = false;
				}

				if (OperationState.RUNNING.equals(task.getOperationState())
						&& task.getEndOperation().compareTo(timeProcess) <= 0) {

					Machine machine = MachineUtil.findMachinByName(machines, task.getOperationJobMachine().getMachine());
					machine.setAvailable(true);
					task.setOperationState(OperationState.SUCCESS);
					iteratorSimulator.remove();
					orderSimulator.remove(task);
					
					machine.setTimeLastRun(timeProcess);
					
					isFinish = true;
					
					Machine newMachine = MachineUtil.findNextMachine(machines, machine.getName());
					if (newMachine != null) {
						newMachine.setAvailable(true);
					}
				} 
			}

		}
		
		long finish = timeProcess.getTime();
		System.out.println("###### FIM finish: " + finish + ", time: " + DateUtil.dateFormat.format(timeProcess));
		
		genericSolution.setTimeEnd(timeProcess);
		
		long totalTimeExecute = finish - start;
		System.out.println("###### TOTAL (finish - start): " + totalTimeExecute);
		
		solution.setObjective(0, Double.parseDouble(String.valueOf(totalTimeExecute)));

		if (betterTime == 0 || totalTimeExecute <= betterTime) {
			betterTime = totalTimeExecute;
			betterSolution = solution;
			System.out.println("************************************************");
			System.out.println("*** -> evaluate - betterTime: " + betterTime);
			System.out.println("*** -> betterSolution: " + betterSolution.getVariables());
			System.out.println("************************************************");
			
			listaGenericSolution.add(genericSolution);
		}
		
	}
	
	private Task findBeforeTask(List<Task> orderSimulator, String beforeMachine, String job, String operation) {
		for (Task task : orderSimulator) {
			if (task.getOperationJobMachine().getMachine().equals(beforeMachine)
					&& task.getOperationJobMachine().getJob().equals(job)
					&& task.getOperationJobMachine().getOperation().equals(operation)) {
				return task;
			}
		}
		return null;
	}
	
	private long timeSimulatorMillisecond(int timeOperation, int timeMachine) {
		
		int timeSimulutor = (timeOperation / timeMachine) * 1000;
		
		return timeSimulutor;
	}
		
	private boolean isAnyOperationPending(List<Task> orderOperations) {

		for (Task simulator : orderOperations) {
			if (!OperationState.SUCCESS.equals(simulator.getOperationState())) {
				return true;
			}
		}
		return false;
	}
	
	private void parseTaskResult(GenericSolution genericSolution, Task task, Date timeProcess, Date endDate) {
		
		boolean isNewMachine = true;
		
		if (!CollectionUtils.isEmpty(genericSolution.getListSchedulerResult())) {
			for (int i = 0; i < genericSolution.getListSchedulerResult().size(); i++) {
				
				SchedulerResult schedulerResult = (SchedulerResult) genericSolution.getListSchedulerResult().get(i);
				
				if (schedulerResult != null && task.getOperationJobMachine().getMachine().equals(schedulerResult.getMachine())) {
					
					TaskResult taskResult = new TaskResult();
					taskResult.setJob(task.getOperationJobMachine().getJob());
					taskResult.setOperation(task.getOperationJobMachine().getOperation());
					taskResult.setBeginOperation(timeProcess);
					taskResult.setEndOperation(endDate);
//					System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++");
//					System.out.println("JOB: " + task.getOperationJobMachine().getJob());
//					System.out.println("Operation: " + task.getOperationJobMachine().getOperation());
//					System.out.println("BeginOperation: " + timeProcess);
//					System.out.println("EndOperation: " + endDate);
//					System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++");
					
					schedulerResult.getListTaskResult().add(taskResult);
					
					isNewMachine = false;
				}
			}
		}

		if (isNewMachine) {
			
			SchedulerResult schedulerResult = new SchedulerResult();

			schedulerResult.setMachine(task.getOperationJobMachine().getMachine());
			schedulerResult.setListTaskResult(new ArrayList<TaskResult>());
			
			TaskResult taskResult = new TaskResult();
			taskResult.setJob(task.getOperationJobMachine().getJob());
			taskResult.setOperation(task.getOperationJobMachine().getOperation());
			taskResult.setBeginOperation(timeProcess);
			taskResult.setEndOperation(endDate);
			
//			System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++");
//			System.out.println("JOB: " + task.getOperationJobMachine().getJob());
//			System.out.println("Operation: " + task.getOperationJobMachine().getOperation());
//			System.out.println("BeginOperation: " + timeProcess);
//			System.out.println("EndOperation: " + endDate);
//			System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++");
			
			schedulerResult.getListTaskResult().add(taskResult);
			
			if (CollectionUtils.isEmpty(genericSolution.getListSchedulerResult())) {
				genericSolution.setListSchedulerResult(new ArrayList<SchedulerResult>());
			}
			
			genericSolution.getListSchedulerResult().add(schedulerResult);
		}
	}

	public List<Jobs1> getJobs1() {
		return jobs1;
	}

	public void setJobs1(List<Jobs1> jobs1) {
		this.jobs1 = jobs1;
	}

	public List<OperationJobMachine> getOperations() {
		return operations;
	}

	public void setOperations(List<OperationJobMachine> operations) {
		this.operations = operations;
	}

	public List<Machine> getMachines() {
		return machines;
	}

	public void setMachines(List<Machine> machines) {
		this.machines = machines;
	}

	public PermutationSolution<Integer> getBetterSolution() {
		return betterSolution;
	}

	public void setBetterSolution(PermutationSolution<Integer> betterSolution) {
		this.betterSolution = betterSolution;
	}

	public long getBetterTime() {
		return betterTime;
	}

	public void setBetterTime(long betterTime) {
		this.betterTime = betterTime;
	}

	public List<Job> getJob() {
		return job;
	}

	public void setJob(List<Job> job) {
		this.job = job;
	}

	public List<GenericSolution> getListaGenericSolution() {
		return listaGenericSolution;
	}

	public void setListaGenericSolution(List<GenericSolution> listaGenericSolution) {
		this.listaGenericSolution = listaGenericSolution;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Map<Integer, String> getJobsOperacao() {
		return jobsOperacao;
	}

	public void setJobsOperacao(Map<Integer, String> jobsOperacao) {
		this.jobsOperacao = jobsOperacao;
	}

}
