package pt.iscte.ads.sgia.enums;

public enum OperationState {
	
	NOT_STARTED("Operation not started!"),
	RUNNING("Operation running!"),
	SUCCESS("Operation completed successfully!"),
	FAIL("Operation completed with failure!");
	
	private String description;

	private OperationState(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public static OperationState getOperationStateByDescription(String description)  {
		
		if (NOT_STARTED.getDescription().equalsIgnoreCase(description)) {
			return OperationState.NOT_STARTED;
		} else if (RUNNING.getDescription().equalsIgnoreCase(description)) {
			return OperationState.RUNNING;
		} else if (SUCCESS.getDescription().equalsIgnoreCase(description)) {
			return OperationState.SUCCESS;
		}  else if (FAIL.getDescription().equalsIgnoreCase(description)) {
			return OperationState.FAIL;
		}
		
		return null;
	}

}