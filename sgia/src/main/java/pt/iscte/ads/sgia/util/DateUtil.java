package pt.iscte.ads.sgia.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class DateUtil {
	
	public static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	public static Date convertStartTime(List<String> startTime ) {
		
		Calendar calendar = Calendar.getInstance();

		if (startTime != null) {
			
			String hoursSent = "";
			String daySent = "";
			
			for (String time : startTime) {
				if (time.contains(":")) {
					hoursSent = time;
				} else {
					daySent = time;
				}
			}
			
			String[] dayFragment = daySent.split("-");
			int year = Integer.parseInt(dayFragment[0]);
			int month = Integer.parseInt(dayFragment[1]);
			int day = Integer.parseInt(dayFragment[2]);
			
			String[] hoursFragment = hoursSent.split(":");
			int hour = Integer.parseInt(hoursFragment[0]);
			int minute = Integer.parseInt(hoursFragment[1]);
			int second = Integer.parseInt(hoursFragment[2]);

			calendar.set(year, month, day, hour, minute, second);
		}
		
		return calendar.getTime();
	}

}
