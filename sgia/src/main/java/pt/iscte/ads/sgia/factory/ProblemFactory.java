package pt.iscte.ads.sgia.factory;

import org.springframework.stereotype.Component;
import org.uma.jmetal.problem.Problem;
import org.uma.jmetal.solution.Solution;

import pt.iscte.ads.sgia.model.AlgorithmRequest;
import pt.iscte.ads.sgia.problem.MakespanFlowShopProblem;
import pt.iscte.ads.sgia.problem.MakspanJobShopProblem;
import pt.iscte.ads.sgia.problem.SimpleJobShopProblem;

@Component
public class ProblemFactory {

	private static final String DETERMINISTIC_JOBSHOP_PROBLEM = "Deterministic Job Shop";
	private static final String FLOWSHOP_PROBLEM = "Flow Shop";
	private static final String OPENSHOP_PROBLEM = "Open Shop";

	@SuppressWarnings("unchecked")
	public <T extends Solution<?>> Problem<T> getInstanceProblem(AlgorithmRequest algorithmRequest) {
		return (Problem<T>) createInstance(algorithmRequest);
	}

	public Problem<?> getInstance(AlgorithmRequest algorithmRequest) {
		return createInstance(algorithmRequest);
	}

	private Problem<?> createInstance(AlgorithmRequest algorithmRequest) {
		switch (algorithmRequest.getProblemType()) {
		case DETERMINISTIC_JOBSHOP_PROBLEM:
			return new SimpleJobShopProblem(algorithmRequest);
		case FLOWSHOP_PROBLEM:
			return new MakespanFlowShopProblem(algorithmRequest);
		case OPENSHOP_PROBLEM:
			if (algorithmRequest.getHeuristic().get(0).equals("random"))
				return new MakspanJobShopProblem(algorithmRequest);
			return new SimpleJobShopProblem(algorithmRequest);
		default:
			break;
		}
//		if (OBJECTIVE_MAKESPAN.equals(algorithmRequest.getObjectives().get(0).getName())
//				&& algorithmRequest.getObjectives().get(0).isChecked()) {
//			return new MakspanJobShopProblem(algorithmRequest);
//		}
		return null;
	}

}
