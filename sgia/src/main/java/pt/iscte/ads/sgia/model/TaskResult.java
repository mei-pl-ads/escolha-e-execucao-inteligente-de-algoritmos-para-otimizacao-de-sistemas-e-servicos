package pt.iscte.ads.sgia.model;

import java.util.Date;

public class TaskResult {
	
	private String job;
	
	private String operation;
	
	private Date beginOperation;
	
	private Date endOperation;

	public String getJob() {
		return job;
	}

	public void setJob(String job) {
		this.job = job;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public Date getBeginOperation() {
		return beginOperation;
	}

	public void setBeginOperation(Date beginOperation) {
		this.beginOperation = beginOperation;
	}

	public Date getEndOperation() {
		return endOperation;
	}

	public void setEndOperation(Date endOperation) {
		this.endOperation = endOperation;
	}

}
