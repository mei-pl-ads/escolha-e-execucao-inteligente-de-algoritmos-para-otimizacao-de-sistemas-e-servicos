package pt.iscte.ads.sgia.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class GenericSolution<T> {

	private String codIdentify;

	private List<T> variables;

	private Date timeStart;

	private Date timeEnd;

	private long consumption;

	private List<SchedulerResult> listSchedulerResult;

	public String getCodIdentify() {
		return codIdentify;
	}

	public void setCodIdentify(String codIdentify) {
		this.codIdentify = codIdentify;
	}

	public List<T> getVariables() {
		return variables;
	}

	public void setVariables(List<T> variables) {
		this.variables = variables;
	}

	public Date getTimeStart() {
		return timeStart;
	}

	public void setTimeStart(Date timeStart) {
		this.timeStart = timeStart;
	}

	public Date getTimeEnd() {
		return timeEnd;
	}

	public void setTimeEnd(Date timeEnd) {
		this.timeEnd = timeEnd;
	}

	public List<SchedulerResult> getListSchedulerResult() {
		return listSchedulerResult;
	}

	public void setListSchedulerResult(List<SchedulerResult> listSchedulerResult) {
		this.listSchedulerResult = listSchedulerResult;
	}

	public long getConsumption() {
		return consumption;
	}

	public void setConsumption(long consumption) {
		this.consumption = consumption;
	}

}
