package pt.iscte.ads.sgia.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.uma.jmetal.solution.Solution;

import pt.iscte.ads.sgia.model.AlgorithmRequest;
import pt.iscte.ads.sgia.model.AlgorithmResult;
import pt.iscte.ads.sgia.service.OptimizationAlgorithmService;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping(path = "/", produces = MediaType.APPLICATION_JSON_VALUE)
public class SGIAController {

	private static final Logger LOGGER = LoggerFactory.getLogger(SGIAController.class);

	@Autowired
	private OptimizationAlgorithmService optimizationAlgorithmService;
	
	@PostMapping("/request")
	public <T extends Solution<?>> AlgorithmResult<?> createRequest(@RequestBody AlgorithmRequest algorithmRequest) {
		LOGGER.info("-> Inicio createRequest: " + algorithmRequest);

		AlgorithmResult<T> algorithmResult = (AlgorithmResult<T>) optimizationAlgorithmService.executeOptimizationAlgorithmSolution(algorithmRequest);
				
		LOGGER.info("-> Aplicação executado com sucesso");
		return algorithmResult;
	}

}