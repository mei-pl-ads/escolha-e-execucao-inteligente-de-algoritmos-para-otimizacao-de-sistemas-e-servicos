package pt.iscte.ads.sgia.model;

import java.util.Date;

import pt.iscte.ads.sgia.enums.OperationState;

public class Simulator {

	private Operation operation;
	
	private OperationState operationState = OperationState.NOT_STARTED;
	
	private double beginOperation;
	
	private double endOperation;
	
	public Simulator(Operation operation) {
		this.operation = operation;
	}

	public Operation getOperation() {
		return operation;
	}

	public void setOperation(Operation operation) {
		this.operation = operation;
	}

	public OperationState getOperationState() {
		return operationState;
	}

	public void setOperationState(OperationState operationState) {
		this.operationState = operationState;
	}

	public double getBeginOperation() {
		return beginOperation;
	}

	public void setBeginOperation(double beginOperation) {
		this.beginOperation = beginOperation;
	}

	public double getEndOperation() {
		return endOperation;
	}

	public void setEndOperation(double endOperation) {
		this.endOperation = endOperation;
	}

}
