package pt.iscte.ads.sgia.model;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

public class Heuristic {
	public List<String> heuristic;
	public String status;

	public List<String> getName() {
		return heuristic;
	}

	public String getStatus() {
		return status;
	}

	public void setName(List<String> name) {
		this.heuristic = name;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public boolean isChecked() {
		return StringUtils.isNotBlank(status) && "true".equalsIgnoreCase(status) ? true : false;
	}
}
