package pt.iscte.ads.sgia.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.uma.jmetal.algorithm.Algorithm;
import org.uma.jmetal.algorithm.multiobjective.nsgaii.NSGAIIBuilder;
import org.uma.jmetal.operator.crossover.CrossoverOperator;
import org.uma.jmetal.operator.crossover.impl.PMXCrossover;
import org.uma.jmetal.operator.mutation.MutationOperator;
import org.uma.jmetal.operator.mutation.impl.PermutationSwapMutation;
import org.uma.jmetal.operator.selection.SelectionOperator;
import org.uma.jmetal.operator.selection.impl.BestSolutionSelection;
import org.uma.jmetal.operator.selection.impl.BinaryTournamentSelection;
import org.uma.jmetal.problem.Problem;
import org.uma.jmetal.solution.Solution;
import org.uma.jmetal.solution.permutationsolution.PermutationSolution;
import org.uma.jmetal.util.comparator.RankingAndCrowdingDistanceComparator;
import org.uma.jmetal.util.fileoutput.SolutionListOutput;
import org.uma.jmetal.util.fileoutput.impl.DefaultFileOutputContext;
import org.uma.jmetal.util.pseudorandom.JMetalRandom;
//import org.uma.jmetal.example.AlgorithmRunner;

import pt.iscte.ads.sgia.factory.ProblemFactory;
import pt.iscte.ads.sgia.factory.SolutionResultFactory;
import pt.iscte.ads.sgia.model.AlgorithmRequest;
import pt.iscte.ads.sgia.model.AlgorithmResult;
import pt.iscte.ads.sgia.model.GenericSolution;
import pt.iscte.ads.sgia.model.Machine;
import pt.iscte.ads.sgia.service.OptimizationAlgorithmService;

@Service
public class OptimizationAlgorithmServiceImpl implements OptimizationAlgorithmService {

	private static final Logger LOGGER = LoggerFactory.getLogger(OptimizationAlgorithmServiceImpl.class);
	private static final String RESULTS_BASE_DIRECTORY = "ICO-Solutions-And-Results/data/";
	private static final String VARIABLES_RESULTS_FILE = "VAR.tsv";
	private static final String OBJ_FUNCTIONS_RESULTS_FILE = "FUN.tsv";

	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	@Autowired
	private ProblemFactory problemFactory;

	@Autowired
	private SolutionResultFactory solutionResultFactory;

	@Override
	public <T extends Solution<?>> AlgorithmResult<?> executeOptimizationAlgorithmSolution(
			AlgorithmRequest algorithmRequest) {

		// MakspanJobShopProblem sjsp = new MakspanJobShopProblem(algorithmRequest);
		createAndAddEqualMachines(algorithmRequest);

		Problem<T> problem = problemFactory.getInstanceProblem(algorithmRequest);

		AlgorithmResult<T> algorithmResult = executeNSGAII(algorithmRequest, problem);

		return algorithmResult;
	}

	private void createAndAddEqualMachines(AlgorithmRequest request) {
		List<Machine> newMachineList = new ArrayList<Machine>();
		int numberOfMachine = request.getMachines().size();
		for (Machine machine : request.getMachines()) {
			if (machine.getMachinesNumber() > 1) {
				for (int i = 0; i < machine.getMachinesNumber() - 1; i++) {
					Machine mach = machine.getNewMachine(machine);
					numberOfMachine += 1;
					mach.setName("machine" + numberOfMachine);
					newMachineList.add(mach);
				}
			}
		}
		request.getMachines().addAll(newMachineList);
	}

	private <T extends Solution<?>> AlgorithmResult<T> executeNSGAII(AlgorithmRequest algorithmRequest,
			Problem<T> problem) {

		Date inicio = new Date();
		System.out.println("################## INICIO EXECUÇÃO GERAL: " + dateFormat.format(inicio));

		// PermutationProblem<PermutationSolution<Integer>> problem;
		Algorithm<List<PermutationSolution<Integer>>> algorithm;
		CrossoverOperator<PermutationSolution<Integer>> crossover;
		MutationOperator<PermutationSolution<Integer>> mutation;
		SelectionOperator<List<PermutationSolution<Integer>>, PermutationSolution<Integer>> selection;

//		crossover = new PMXCrossover(algorithmRequest.getaParams().getCrossoverProbability());
//		//crossover = new TwoPointCrossover<Integer>(0.9);
//		
//		//double mutationProbability = 0.2;
//		mutation = new PermutationSwapMutation<Integer>(algorithmRequest.getaParams().getMutationProbability());
//		//mutation = new SimpleRandomMutation(mutationProbability);
//
//		selection = new BinaryTournamentSelection<PermutationSolution<Integer>>(
//				new RankingAndCrowdingDistanceComparator<PermutationSolution<Integer>>());
//
//		// Número de soluções
//		int populationSize = algorithmRequest.getaParams().getPopulationSize();

		// crossover = new PMXCrossover(0.95);
		crossover = new PMXCrossover(algorithmRequest.getaParams().getCrossoverProbability());

		// double mutationProbability = 0.05;
		double mutationProbability = algorithmRequest.getaParams().getMutationProbability();

		mutation = new PermutationSwapMutation<Integer>(mutationProbability);

		selection = new BestSolutionSelection<PermutationSolution<Integer>>(
				new RankingAndCrowdingDistanceComparator<PermutationSolution<Integer>>());

		// Número de soluções
		// int populationSize = 6;
		int populationSize = algorithmRequest.getaParams().getPopulationSize();

		algorithm = new NSGAIIBuilder<PermutationSolution<Integer>>((Problem<PermutationSolution<Integer>>) problem,
				crossover, mutation, populationSize).setSelectionOperator(selection)
						.setMaxEvaluations(algorithmRequest.getaParams().getIterations() * populationSize).build();

		// AlgorithmRunner algorithmRunner = new
		// AlgorithmRunner.Executor(algorithm).execute();
		algorithm.run();

		List<PermutationSolution<Integer>> population = algorithm.getResult();
		// long computingTime = algorithmRunner.getComputingTime();

		new SolutionListOutput(population)
				.setVarFileOutputContext(new DefaultFileOutputContext(RESULTS_BASE_DIRECTORY + VARIABLES_RESULTS_FILE))
				.setFunFileOutputContext(
						new DefaultFileOutputContext(RESULTS_BASE_DIRECTORY + OBJ_FUNCTIONS_RESULTS_FILE))
				.print();

		LOGGER.info("Random seed: " + JMetalRandom.getInstance().getSeed());
		LOGGER.info("Objectives values have been written to file FUN.tsv");
		LOGGER.info("Variables values have been written to file VAR.tsv");

		List<GenericSolution> listGenericSolution = solutionResultFactory.getSolutionResult(algorithmRequest, problem,
				population);

		AlgorithmResult<T> algorithmResult = new AlgorithmResult<T>(algorithmRequest, problem, population,
				listGenericSolution);

		System.out.println("################## hora de INICIO: " + dateFormat.format(inicio));
		System.out.println("################## FIM EXECUÇÃO GERAL: " + dateFormat.format(new Date()));

		algorithmResult.setAlgorithmRequest(null);
		algorithmResult.setProblem(null);

		return algorithmResult;
	}
}
