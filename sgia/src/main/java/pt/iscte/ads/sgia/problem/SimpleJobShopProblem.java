package pt.iscte.ads.sgia.problem;

import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.commons.lang3.time.DateUtils;
import org.springframework.util.CollectionUtils;
import org.uma.jmetal.problem.permutationproblem.impl.AbstractIntegerPermutationProblem;
import org.uma.jmetal.solution.permutationsolution.PermutationSolution;
import org.uma.jmetal.solution.permutationsolution.impl.IntegerPermutationSolution;

import pt.iscte.ads.sgia.model.AlgorithmRequest;
import pt.iscte.ads.sgia.model.Allocation;
import pt.iscte.ads.sgia.model.GenericSolution;
import pt.iscte.ads.sgia.model.Heuristic;
import pt.iscte.ads.sgia.model.Job;
import pt.iscte.ads.sgia.model.Jobs1;
import pt.iscte.ads.sgia.model.Machine;
import pt.iscte.ads.sgia.model.Objective;
import pt.iscte.ads.sgia.model.Operation;
import pt.iscte.ads.sgia.model.SchedulerResult;
import pt.iscte.ads.sgia.model.Task;
import pt.iscte.ads.sgia.model.TaskResult;
import pt.iscte.ads.sgia.simulator.JobShopSimulator;
import pt.iscte.ads.sgia.util.DateUtil;

public class SimpleJobShopProblem extends AbstractIntegerPermutationProblem {

	private static final long serialVersionUID = 4907655737227857332L;

	// Heuristics
	private static final String FCFS = "first";
	private static final String SPTF = "shortest";

	private static final String RA = "random";

	// Objectives
	private static final String MM = "Minimize the makespan";
	private static final String MC = "Minimize total consumption";
	
	private static final String OS = "Open Shop";
	private static final String DJB = "Deterministic Job Shop";

	// Problem Attributes
	protected int numberOfOperations;
	protected int numberOfMachines;
	protected double[][] processingTimesMatrix;
	protected int[][] orderMatrix;

	private List<Jobs1> jobs;
	private List<Operation> operations;
	private List<Machine> machines;
	private String heuristic;
	private List<Objective> objectives;
	private Date startDate;
	private String problemType;

	private Date startTime;

	// New ideia for save the solutions
	Map<Integer, GenericSolution> solutionsMap = new HashMap<Integer, GenericSolution>();

	List<GenericSolution> listaGenericSolution = new ArrayList<GenericSolution>();

	public SimpleJobShopProblem(AlgorithmRequest algorithmRequest) {
		List<Operation> operations = new ArrayList<Operation>();
		for (Jobs1 job : algorithmRequest.getJobs1()) {
			for (Operation operation : job.getOperations()) {
				operation.setJob(job);
				operations.add(operation);
			}
		}
		setObjectives(algorithmRequest.getObjectives());
		setOperations(operations);
		setMachines(algorithmRequest.getMachines());
		setJobs(algorithmRequest.getJobs1());
		setNumberOfOperations(operations.size());
		setNumberOfMachines(algorithmRequest.getMachines().size());
		setProcessingTimesMatrix(operations, algorithmRequest.getMachines());
		setStartTime(DateUtil.convertStartTime(algorithmRequest.getStartTime()));
		setNumberOfVariables(numberOfOperations);
		setNumberOfObjectives(objectives.size());
		setHeuristic(algorithmRequest.getHeuristic());
		setName("JobShopProblem");
		String date = algorithmRequest.getStartTime().get(1);
		String hour = algorithmRequest.getStartTime().get(0);
		String dateHour = date + " " + hour;
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date realDate = null;
		try {
			realDate = formatter.parse(dateHour);
		} catch (ParseException e) {
			e.printStackTrace();
		}
	    this.setStartDate(realDate);
		// 1 - Operations Precedence Constraint; Job Priority Constraint; 3 - Job DueDate Constraint
	    if (realDate != null) {
			setNumberOfConstraints(3);
	    } else {
			setNumberOfConstraints(2);
	    }
	    
	    setProblemType(algorithmRequest.getProblemType());
	}

	@Override
	public int getLength() {
		return numberOfOperations;
	}

	@Override
	public void evaluate(PermutationSolution<Integer> solution) {
		List<Operation> orderOperations = new ArrayList<Operation>();
		for (Operation operation : operations) {
			operation.setEndDate(null);
			operation.setStartDate(null);
			operation.startTime = 0;
			operation.endTime = 0;
		}
		for (Machine machine : machines) {
			machine.setTimeLastRun(null);
		}
		for (int i = 0; i < solution.getVariables().size(); i++) {
			Operation operation = this.operations.get(solution.getVariables().get(i));
			orderOperations.add(operation);
		}

		List<Allocation> allocations = new ArrayList<Allocation>();
		allocations = allocate(orderOperations, allocations);

		GenericSolution genericSolution = new GenericSolution();
		genericSolution.setCodIdentify(solution.getVariables().toString());
		genericSolution.setVariables(solution.getVariables());
		if (this.getStartDate() != null) {
			genericSolution.setTimeStart(this.getStartDate());
		} else {
			genericSolution.setTimeStart(startTime);
		}

		JobShopSimulator jobShopSimulator = new JobShopSimulator();
		int nObjective = 0;

		Map<Integer, List<Operation>> executionOrderOfOperations = new HashMap<Integer, List<Operation>>();

		// 1 - Calculate Total Makespan
		if (isObjective(MM)) {
			resetAllocations(allocations);
			resetMachines();
			resetOperations();
			System.out.println("Vamos simular as seguintes alocações:");
			System.out.println("Allocations: " + allocations);
			long makespan = jobShopSimulator.simulateMakespan(allocations, jobs, genericSolution,
					executionOrderOfOperations);
			solution.setObjective(nObjective, makespan);
			nObjective++;
		}

		// 2 - Calculate Total Consumption
		if (isObjective(MC)) {
			resetAllocations(allocations);
			resetMachines();
			resetOperations();
			long consumption = jobShopSimulator.simulateConsumption(allocations, jobs, genericSolution,
					executionOrderOfOperations);
			solution.setObjective(nObjective, consumption);
		}

		setJobsEndDate();
		
		if (this.getProblemType().equals(DJB)) {
			int precedencePen = getPrecedencePen(executionOrderOfOperations,orderOperations);
			solution.setConstraint(0, precedencePen);
					
			int jobPen = getJobPen();
			solution.setConstraint(1, jobPen);
			
			String violatedConstraints = "Constraints violadas:\nOrdem de precedencia de operações - " + precedencePen + "\nPrioridade de finalização dos jobs - " + jobPen;
	
			if (this.getStartDate() != null) {
				int datePen = getFinishDatePen();
				solution.setConstraint(2, datePen);
				violatedConstraints += "\nData de finalização dos job - " + datePen;
	
			}
			System.out.println(violatedConstraints);
		}
			
		// save solution
		solutionsMap.put(solution.hashCode(), genericSolution);
	}
	
	private int getJobPen() {
		int jobPen = 0;
		// Mapa com jobs e data de termino
		Map<Jobs1, Date> finishedJobs = new HashMap<>();
		for (Operation o : this.operations) {
			Date date = o.endDate;
			if(!finishedJobs.containsKey(o.getJob()) || finishedJobs.get(o.getJob()).after(date)) {
				finishedJobs.put(o.getJob(),date);
			}
		}
		
		for (Jobs1 job : this.jobs) {
			for (Jobs1 job2 : this.jobs) {
				if (job != job2) {
					if (job.getPriority() > job2.getPriority() && finishedJobs.get(job).before(finishedJobs.get(job2))) {
						jobPen += 20;
					}
				}
			}
		}
		return jobPen;
	}
	private int getFinishDatePen() {
		  int datePen = 0;
		  Map<Jobs1, Date> finishedJobs = new HashMap<>();
		  for (Operation o : this.operations) {
			 Date date = o.endDate;
			 if(!finishedJobs.containsKey(o.getJob()) || finishedJobs.get(o.getJob()).after(date)) {
				 finishedJobs.put(o.getJob(),date);
			 }
		  }
		  
		  for (Map.Entry<Jobs1, Date> entry : finishedJobs.entrySet()) {
			  if (!"".equals(entry.getKey().getDueto())) {
				String minutes = entry.getKey().getDueto().substring(0, 2);
				String seconds = entry.getKey().getDueto().substring(3, 5);
				if (!minutes.isEmpty() && !seconds.isEmpty()) {

					int integerHours = Integer.valueOf(minutes);
					int integerMinutes = Integer.valueOf(seconds);

					if (integerHours != 00 && integerMinutes != 00) {
						Date finishDate = DateUtils.addHours(this.startDate, integerHours);
						finishDate = DateUtils.addMinutes(finishDate, integerMinutes);
						if (finishDate.before(entry.getValue())) {
							datePen += 10;
						}
					}
				} 
			}
		  }
		
		  return datePen;
	}
	
	private int getPrecedencePen(Map<Integer, List<Operation>> executionOrderOfOperations, List<Operation> orderOperations) {
		int precedencePen = 0;
		
		List<Operation> executedOperations = new ArrayList<>();
		for (Map.Entry<Integer, List<Operation>> entry : executionOrderOfOperations.entrySet()) {
			for (Operation o : entry.getValue()) {
				executedOperations.add(o);
			}
		}
		
		Map<Jobs1, List<Operation>> operationJobExecuted = new HashMap<>();
		for (Operation o : executedOperations) {
			Jobs1 actualJob = o.getJob();
			if (!operationJobExecuted.containsKey(actualJob)) {
				List<Operation> newOperations = new ArrayList<>();
				newOperations.add(o);
				operationJobExecuted.put(o.getJob(), newOperations);
			} else {
				List<Operation> newOperations = operationJobExecuted.get(actualJob);
				newOperations.add(o);
				operationJobExecuted.put(o.getJob(), newOperations);
			}
		}

		Map<Jobs1, List<Operation>> operationJobOrdered = new HashMap<>();
		for (Operation o : operations) {
			Jobs1 actualJob = o.getJob();
			if (!operationJobOrdered.containsKey(actualJob)) {
				List<Operation> newOperations = new ArrayList<>();
				newOperations.add(o);
				operationJobOrdered.put(o.getJob(), newOperations);
			} else {
				List<Operation> newOperations = operationJobOrdered.get(actualJob);
				newOperations.add(o);
				operationJobOrdered.put(o.getJob(), newOperations);
			}
		}

		for (Map.Entry<Jobs1, List<Operation>> entry : operationJobOrdered.entrySet()) {
			for (int i = 0; i < entry.getValue().size(); i++) {
				if ((i + 1) < entry.getValue().size()) {
					entry.getValue().get(i).setAfterOperation(entry.getValue().get(i + 1));
					;
				}
			}
		}

		for (Map.Entry<Jobs1, List<Operation>> entry : operationJobExecuted.entrySet()) {
			for (int i = 0; i < entry.getValue().size(); i++) {
				if ((i + 1) < entry.getValue().size()) {
					if (entry.getValue().get(i).getAfterOperation() == null) {
						precedencePen += 50;
					}
				}
			}
		}
		
		
		return precedencePen;
	}

	private void setJobsEndDate() {
		for (Jobs1 job : jobs) {
			List<Operation> operations = job.getOperations();
			Date date = new Date(0);
			for (Operation operation : operations) {
				if (operation.getEndDate().after(date))
					date = operation.getEndDate();
			}
			job.setEndDate(date);
		}
	}

	private void resetAllocations(List<Allocation> allocations) {
		for (Allocation allocation : allocations) {
			allocation.resetAllocation();
		}
	}

	private void resetMachines() {
		for (Machine machine : machines) {
			machine.setAvailable(true);
		}
	}

	private void resetOperations() {
		for (Operation operation : operations) {
			operation.startDate = null;
			operation.endDate = null;
			operation.isExecuting = false;
			operation.isFinished = false;
		}
	}

	private List<Allocation> allocate(List<Operation> orderOperations, List<Allocation> allocations) {
		if (FCFS.equals(heuristic))
			allocations = allocateMachinesFCFS(orderOperations);
		else if (SPTF.equals(heuristic)) {
			allocations = allocateMachinesFCFS(orderOperations);
			List<Allocation> newAllocations = new ArrayList<Allocation>();
			while (newAllocations.size() != allocations.size()) {
				Allocation allocation = null;
				for (Allocation alloc : allocations) {
					if (allocation == null && !newAllocations.contains(alloc))
						allocation = alloc;
					double proccessingTime = alloc.getProcessingTime();
					if (allocation != null && alloc.getProcessingTime() < allocation.getProcessingTime() && !newAllocations.contains(alloc)) {
						allocation = alloc;
					}
				}
				newAllocations.add(allocation);
			}
			allocations = newAllocations;
		}
		return allocations;
	}

	public List<Allocation> allocateMachinesFCFS(List<Operation> operations) {
		List<Allocation> allocations = new ArrayList<>();
		for (int i = 0; i < operations.size(); i++) {
			Operation operation = operations.get(i);
			Machine machine = this.machines.get(0);

			// caso em que a operação não tem uma maquina especifica para ser executada
			if (operation.machine.get(0).equals("any")) {
				Random rand = new Random();
				int upperbound = numberOfMachines;
				int randomValue = rand.nextInt(upperbound);

				machine = this.machines.get(randomValue);
			} else if (operation.machine.size() == 1) {
				machine = getPossibleMachine(operation.machine.get(0));
			} else {
				List<Machine> machineList = getPossibleMachinesList(operation.machine);

				Random rand = new Random();
				int upperbound = machineList.size();
				int randomValue = rand.nextInt(upperbound);

				machine = machineList.get(randomValue);
			}
			double proccessingTime = processingTimesMatrix[operation.getId()][machine.getMachineId()];
			Allocation allocation = new Allocation(operation, machine, proccessingTime);
			allocations.add(allocation);
		}
		return allocations;
	}

	public List<Allocation> allocateMachinesSPC(List<Operation> operations) {
		List<Allocation> allocations = new ArrayList<>();
		for (int i = 0; i < operations.size(); i++) {
			Operation operation = operations.get(i);
			Machine machine = this.machines.get(0);

			// caso em que a operação não tem uma maquina especifica para ser executada
			if (operation.machine.get(0).equals("any")) {
				machine = getLessProccessingTime(operation, this.machines);
			} else if (operation.machine.size() == 1) {
				machine = getPossibleMachine(operation.machine.get(0));
			} else {
				List<Machine> machineList = getPossibleMachinesList(operation.machine);

				machine = getLessProccessingTime(operation, machineList);

			}
			double proccessingTime = processingTimesMatrix[operation.getId()][machine.getMachineId()];
			Allocation allocation = new Allocation(operation, machine, proccessingTime);
			allocations.add(allocation);
		}
		return allocations;
	}

	private List<Machine> getPossibleMachinesList(List<String> machineNames) {
		List<Machine> machineList = new ArrayList<>();
		for (int j = 0; j < machineNames.size(); j++) {
			for (int y = 0; y < numberOfMachines; y++) {
				if (machineNames.get(j).equals(this.machines.get(y).getName())) {
					machineList.add(this.machines.get(y));
				}
			}
		}
		return machineList;
	}

	private Machine getPossibleMachine(String machineName) {
		Machine machine = null;
		for (int i = 0; i < numberOfMachines; i++) {
			if (this.machines.get(i).getName().equals(machineName)) {
				machine = this.machines.get(i);
			}
		}
		return machine;
	}

	private Machine getLessProccessingTime(Operation operation, List<Machine> machines) {
		double proccessingTime = 0.0;
		Machine machine = null;
		for (int i = 0; i < machines.size(); i++) {
			double actualProccessingTime = processingTimesMatrix[operation.getId()][machines.get(i).getMachineId()];
			if (machine == null) {
				machine = machines.get(i);
				proccessingTime = actualProccessingTime;
			} else if (proccessingTime > actualProccessingTime) {
				machine = machines.get(i);
				proccessingTime = actualProccessingTime;
			}
		}

		return machine;
	}

	public int getNumberOfOperations() {
		return numberOfOperations;
	}

	public int getNumberOfMachines() {
		return numberOfMachines;
	}

	public double[][] getProcessingTimesMatrix() {
		return processingTimesMatrix;
	}

	public int[][] getOrderMatrix() {
		return orderMatrix;
	}

	public void setNumberOfOperations(int numberOfOperations) {
		this.numberOfOperations = numberOfOperations;
	}

	public void setNumberOfMachines(int numberOfMachines) {
		this.numberOfMachines = numberOfMachines;
	}

	public void setProcessingTimesMatrix(List<Operation> listOfOperations, List<Machine> listOfMachines) {
		processingTimesMatrix = new double[numberOfOperations][numberOfMachines];
		for (int i = 0; i < numberOfOperations; i++) {
			for (int j = 0; j < numberOfMachines; j++) {
				processingTimesMatrix[i][j] = (double) listOfOperations.get(i).getValue()
						/ listOfMachines.get(j).getValue();
			}
		}
	}

	public void setOrderMatrix(int[][] orderMatrix) {
		this.orderMatrix = orderMatrix;
	}

	public List<Operation> getOperations() {
		return operations;
	}

	public List<Machine> getMachines() {
		return machines;
	}

	public void setOperations(List<Operation> operations) {
		this.operations = operations;
	}

	public void setMachines(List<Machine> machines) {
		this.machines = machines;
	}

	public List<Jobs1> getJobs() {
		return jobs;
	}

	public void setJobs(List<Jobs1> jobs) {
		this.jobs = jobs;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public String getHeuristic() {
		return heuristic;
	}

	public void setHeuristic(List<String> heuristic) {
		this.heuristic = heuristic.get(0);
	}

	public List<Objective> getObjectives() {
		return objectives;
	}

	public void setObjectives(List<Objective> objectives) {
		this.objectives = new ArrayList<Objective>();
		for (Objective objective : objectives) {
			if (objective.isChecked())
				this.objectives.add(objective);
		}
	}

	public boolean isObjective(String objectiveName) {
		for (Objective objective : objectives) {
			if (objective.isChecked())
				if (objectiveName.equals(objective.getName()))
					return true;
		}
		return false;
	}

	public List<GenericSolution> getListaGenericSolution() {
		return listaGenericSolution;
	}

	public void setListaGenericSolution(List<GenericSolution> listaGenericSolution) {
		this.listaGenericSolution = listaGenericSolution;
	}

	public Map<Integer, GenericSolution> getSolutionsMap() {
		return solutionsMap;
	}

	public void setSolutionsMap(Map<Integer, GenericSolution> solutionsMap) {
		this.solutionsMap = solutionsMap;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	
	@Override
	public PermutationSolution<Integer> createSolution() {
	    return new IntegerPermutationSolution2(getLength(), getNumberOfObjectives(), getNumberOfConstraints()) ;
	  }

	public String getProblemType() {
		return problemType;
	}

	public void setProblemType(String problemType) {
		this.problemType = problemType;
	}

}
