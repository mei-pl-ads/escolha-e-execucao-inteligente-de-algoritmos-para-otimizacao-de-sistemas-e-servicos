package pt.iscte.ads.sgia.model;

import org.apache.commons.lang3.StringUtils;

public class Objective {
	
	private String name;

	private String status;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public boolean isChecked() {
		return StringUtils.isNotBlank(status) && "true".equalsIgnoreCase(status) ? true : false;
	}

}
