package pt.iscte.ads.sgia.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import pt.iscte.ads.sgia.model.Machine;

public class MachineUtil {

	public static Machine findMachinByName(List<Machine> machines, String nameMachine) {
		
		for (Machine machine : machines) {
			if (machine.getName().equals(nameMachine)) {
				return machine;
			}
		}

		return null;
	}
	
	public static Date findBeforeMachineTimeLastRun(List<Machine> machines, String nameMachine) {

		for (int i = 0; i < machines.size(); i++) {
			Machine machine = machines.get(i);
			if (machine.getName().equals(nameMachine) && i > 0) {
				return machines.get(i-1).getTimeLastRun();
			}
		}

		return null;
	}
	
	public static Machine findBeforeMachine(List<Machine> machines, String nameMachine) {

		for (int i = 0; i < machines.size(); i++) {
			Machine machine = machines.get(i);
			if (i > 0) {
				int before = i-1;
				if (machine.getName().equals(nameMachine)) {
					return machines.get(before);
				}
			}
		}

		return null;
	}
	
	public static Machine findNextMachine(List<Machine> machines, String nameMachine) {

		for (int i = 0; i < machines.size(); i++) {
			Machine machine = machines.get(i);
			int next = i+1;
			if (machine.getName().equals(nameMachine) && (next < machines.size())) {
				return machines.get(next);
			}
		}

		return null;
	}
	
	public static boolean containOptionAny(List<String> machines) {
		
		for (String nameMachine : machines) {
			if ("any".equalsIgnoreCase(nameMachine)) {
				return true;
			}
		}

		return false;
	}
	
	public static Machine getRandomMachine(List<Machine> machines, List<String> machineOptions) {
		
		List<Machine> machinesAvailable = new ArrayList<Machine>();
		
		if (containOptionAny(machineOptions)) {
			machinesAvailable.addAll(machines);
		} else {
			for (String nameMachine : machineOptions) {
				Machine machine = findMachinByName(machines, nameMachine);
				if (machine != null) {
					machinesAvailable.add(machine);
				}
			}
		}

		Random randomGenerator = new Random();
		
		int index = randomGenerator.nextInt(machinesAvailable.size());

		return machinesAvailable.get(index);
	}

}
