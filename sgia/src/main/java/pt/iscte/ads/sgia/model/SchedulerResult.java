package pt.iscte.ads.sgia.model;

import java.util.List;

public class SchedulerResult {
	
	private String machine;
	
	private List<TaskResult> listTaskResult;
	
	private double[] objectives;

	public String getMachine() {
		return machine;
	}

	public void setMachine(String machine) {
		this.machine = machine;
	}
	
	public List<TaskResult> getListTaskResult() {
		return listTaskResult;
	}

	public void setListTaskResult(List<TaskResult> listTaskResult) {
		this.listTaskResult = listTaskResult;
	}

	public double[] getObjectives() {
		return objectives;
	}

	public void setObjectives(double[] objectives) {
		this.objectives = objectives;
	}

}
