package pt.iscte.ads.sgia.model;

import java.util.List;

import org.uma.jmetal.problem.Problem;

public class AlgorithmResult<T> {
	
	private AlgorithmRequest algorithmRequest;
	private Problem<?> problem;
	private List<?> result;
	private List<String> objetiveResult;
	private List<String> variablesResult;
	
	private List<GenericSolution> listGenericSolution;
	
	public AlgorithmResult(AlgorithmRequest algorithmRequest, Problem<?> problem, List<?> result) {
		super();
		this.algorithmRequest = algorithmRequest;
		this.problem = problem;
		this.result = result;
	}
	
	public AlgorithmResult(AlgorithmRequest algorithmRequest, Problem<?> problem, List<?> result, List<GenericSolution> listGenericSolution) {
		super();
		this.algorithmRequest = algorithmRequest;
		this.problem = problem;
		this.result = result;
		this.listGenericSolution = listGenericSolution;
	}

	public AlgorithmRequest getAlgorithmRequest() {
		return algorithmRequest;
	}

	public void setAlgorithmRequest(AlgorithmRequest algorithmRequest) {
		this.algorithmRequest = algorithmRequest;
	}

	public Problem<?> getProblem() {
		return problem;
	}

	public void setProblem(Problem<?> problem) {
		this.problem = problem;
	}

	public List<?> getResult() {
		return result;
	}

	public void setResult(List<?> result) {
		this.result = result;
	}

	public List<String> getObjetiveResult() {
		return objetiveResult;
	}

	public void setObjetiveResult(List<String> objetiveResult) {
		this.objetiveResult = objetiveResult;
	}

	public List<String> getVariablesResult() {
		return variablesResult;
	}

	public void setVariablesResult(List<String> variablesResult) {
		this.variablesResult = variablesResult;
	}

	public List<GenericSolution> getListGenericSolution() {
		return listGenericSolution;
	}

	public void setListGenericSolution(List<GenericSolution> listGenericSolution) {
		this.listGenericSolution = listGenericSolution;
	}

}
