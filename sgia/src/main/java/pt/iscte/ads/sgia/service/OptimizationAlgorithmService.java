package pt.iscte.ads.sgia.service;

import org.uma.jmetal.solution.Solution;

import pt.iscte.ads.sgia.model.AlgorithmRequest;
import pt.iscte.ads.sgia.model.AlgorithmResult;

public interface OptimizationAlgorithmService {

	public <T extends Solution<?>> AlgorithmResult<?> executeOptimizationAlgorithmSolution(AlgorithmRequest algorithmRequest);

	
}
