package pt.iscte.ads.sgia.model;

import java.util.Date;
import java.util.List;

public class Jobs1 {
	
	private String name;
	
	private int priority;
	
	private String dueto;
   
	private List<Operation> operations;
	
	public boolean isExecuting = false;
	
	private Date endDate;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public String getDueto() {
		return dueto;
	}

	public void setDueto(String dueto) {
		this.dueto = dueto;
	}

	public List<Operation> getOperations() {
		return operations;
	}

	public void setOperations(List<Operation> operations) {
		this.operations = operations;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

}
