package pt.iscte.ads.sgia.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Job {

	private String name;

	private Integer value;

	private Integer priority;

	private String dueto;
	
	private Boolean isFinished;

	private Map<String, Object> additionalProperties = new HashMap<String, Object>();
	
	private List<Operation> operations;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getValue() {
		return value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}

	public Integer getPriority() {
		return priority;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	public String getDueto() {
		return dueto;
	}

	public void setDueto(String dueto) {
		this.dueto = dueto;
	}
	
	public Boolean getIsFinished() {
		return isFinished;
	}

	public void setIsFinished(Boolean isFinished) {
		this.isFinished = isFinished;
	}

	public Map<String, Object> getAdditionalProperties() {
		return additionalProperties;
	}

	public void setAdditionalProperties(Map<String, Object> additionalProperties) {
		this.additionalProperties = additionalProperties;
	}
	
	public List<Operation> getOperations() {
		return operations;
	}

	public void setOperations(List<Operation> operations) {
		this.operations = operations;
	}

}