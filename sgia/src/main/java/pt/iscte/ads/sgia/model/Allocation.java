package pt.iscte.ads.sgia.model;

public class Allocation {

	private Operation operation;

	private Machine machine;

	private double processingTime;

	public Allocation(Operation operation, Machine machine, double processingTime) {
		this.operation = operation;
		this.machine = machine;
		this.processingTime = processingTime;
	}

	public Operation getOperation() {
		return operation;
	}

	public Machine getMachine() {
		return machine;
	}

	public double getProcessingTime() {
		return processingTime;
	}

	public void setOperation(Operation operation) {
		this.operation = operation;
	}

	public void setMachine(Machine machine) {
		this.machine = machine;
	}

	public void setProcessingTime(double processingTime) {
		this.processingTime = processingTime;
	}

	public void resetAllocation() {
		resetOperation();
		resetMachine();
	}

	private void resetMachine() {
		machine.setAvailable(true);
	}

	private void resetOperation() {
		operation.startDate = null;
		operation.endDate = null;
		operation.isExecuting = false;
		operation.isFinished = false;
	}

	@Override
	public String toString() {
		return "Allocation: \n-> " + operation.getJob().getName() + " - " + operation.getName() + "\n-> "
				+ machine.getName();
	}

}
