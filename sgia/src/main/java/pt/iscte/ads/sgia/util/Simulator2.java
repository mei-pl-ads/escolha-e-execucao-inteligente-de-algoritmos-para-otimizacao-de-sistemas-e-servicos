package pt.iscte.ads.sgia.util;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.uma.jmetal.solution.permutationsolution.PermutationSolution;

import pt.iscte.ads.sgia.enums.OperationState;
import pt.iscte.ads.sgia.model.Allocation;
import pt.iscte.ads.sgia.model.Machine;
import pt.iscte.ads.sgia.model.Operation;
import pt.iscte.ads.sgia.model.Simulator;
import pt.iscte.ads.sgia.problem.SimpleJobShopProblem;

public class Simulator2 {

	private static void simulate(List<Allocation> allocationList, List<Machine> machines) {
		double startDate = 0.0;
		while (isAnyOperationPending(allocationList)) {
/*
			for (Allocation allocation : allocationList) {
				Machine machine = machines.get(machines.indexOf(allocation.getOperation().getAssignedMachine()));

				if (OperationState.NOT_STARTED.equals(allocation.getOperation().getOperationState())
						&& machine.getIsAvailable()) {
					System.out.println("******> LER - Operation.getName(): " + allocation.getOperation().getName()
							+ ", machine.getName(): " + machine.getName() + " esta livre: "
							+ (machine.getIsAvailable()));
					machine.setIsAvailable(false);
					allocation.getOperation().setOperationState(OperationState.RUNNING);
					Date date = new Date();
					Calendar cal = Calendar.getInstance();
					cal.setTime(date);
					cal.add(Calendar.SECOND, Double.valueOf(startDate).intValue());

					allocation.getOperation().setFormattedStartDate(cal.getTime().toLocaleString());

					cal.add(Calendar.SECOND,
							Double.valueOf(startDate + allocation.getOperation().getValue()).intValue());
					allocation.getOperation().setFormattedEndDate(cal.getTime().toLocaleString());

					System.out.println(
							"++++++> ALTERAR - Iniciou a operation.getName(): " + allocation.getOperation().getName()
									+ ", beginDate: " + allocation.getOperation().getStartDate() + ", endDate2: ");
				}
			}
			Boolean finish = false;
			Iterator<Allocation> iteratorAllocation = allocationList.iterator();
			while (iteratorAllocation.hasNext()) {
				Allocation allocation = iteratorAllocation.next();

				Date now = new Date();
				if (OperationState.RUNNING.equals(allocation.getOperation().getOperationState())
						&& allocation.getOperation().getEndDate() <= startDate) {
					System.out.println("------> FINALIZOU a operation.getName(): " + allocation.getOperation().getName()
							+ " as " + now);
					Machine machine = machines.get(machines.indexOf(allocation.getOperation().getAssignedMachine()));
					machine.setIsAvailable(true);
					allocation.getOperation().setOperationState(OperationState.SUCCESS);
					iteratorAllocation.remove();
					allocationList.remove(allocation);
					finish = true;
				}
			}
			if (!finish)
				startDate += 1;*/
		}
	}

	public static boolean isAnyOperationPending(List<Allocation> allocationList) {

		for (Allocation allocation : allocationList) {
			if (!OperationState.SUCCESS.equals(allocation.getOperation().getOperationState())) {
				return true;
			}
		}
		return false;
	}
}
