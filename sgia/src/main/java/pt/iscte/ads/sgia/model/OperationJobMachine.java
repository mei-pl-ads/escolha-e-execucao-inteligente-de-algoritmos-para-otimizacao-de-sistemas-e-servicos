package pt.iscte.ads.sgia.model;

import java.util.List;

import pt.iscte.ads.sgia.enums.OperationState;

public class OperationJobMachine {
	
	private int id;
	
	private OperationState operationState = OperationState.NOT_STARTED;

	private String operation;
	
	private String job;
    
	private String machine;
    
	private int order;
    
	private String preemptive;
    
	private int value;
   
	private String after;
	
	private List<String> machineOptions;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public OperationState getOperationState() {
		return operationState;
	}

	public void setOperationState(OperationState operationState) {
		this.operationState = operationState;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operacao) {
		this.operation = operacao;
	}

	public String getJob() {
		return job;
	}

	public void setJob(String job) {
		this.job = job;
	}

	public String getMachine() {
		return machine;
	}

	public void setMachine(String machine) {
		this.machine = machine;
	}

	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	public String getPreemptive() {
		return preemptive;
	}

	public void setPreemptive(String preemptive) {
		this.preemptive = preemptive;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public String getAfter() {
		return after;
	}

	public void setAfter(String after) {
		this.after = after;
	}

	public List<String> getMachineOptions() {
		return machineOptions;
	}

	public void setMachineOptions(List<String> machineOptions) {
		this.machineOptions = machineOptions;
	}

}
