
import convertToChromosome from "../src/converter"

function optimisate(problem) {
    // console.log("optimisator")
  let population = []
  let timespans = []
  for (let i = 0; i < problem.aParams.populationSize; i++) {
    let chromosome = convertToChromosome(problem);
    // console.log(chromosome);
    population.push(chromosome)
    timespans.push(evaluateTimespan(chromosome));
  }

  let minTimespan = Number.MAX_SAFE_INTEGER
  let minIndex = 0
  for (let i = 0; i < problem.aParams.populationSize; i++) {
    if (timespans[i] < minTimespan || timespans[i] > 0) {
      minTimespan = timespans[i];
      minIndex = i;
    }
  }

//   console.log(population[minIndex])
  return population[minIndex]



  // return time span for chromosome in miliseconds
  function evaluateTimespan(chromosome) {
    // initialise machines start time - all machines starts at startDateTime
    let machinesTime = [];
    for (let m = 0; m < problem.machines.length; m++) {
      machinesTime.push(0);
    }
    for (let m = 0; m < problem.machines.length; m++) {
      let operationsAssignedForMachine = chromosome.filter(
        (item) => item.machine == m
      )
      for (let o = 0; o < operationsAssignedForMachine.length; o++) {
        let jobIndex = operationsAssignedForMachine[o].job;
        let operationIndex = operationsAssignedForMachine[o].operation;
        let operationValue =
          problem.jobs1[jobIndex].operations[operationIndex].value;
        // time for operation process in assigned machine in miliseconds
        let leadTime = (1000 * operationValue) / problem.machines[m].value;
        machinesTime[m] = +leadTime;
      }
    }
    let timespan = Math.max(...machinesTime)
    // console.log("Timespan evaluator")
    // console.log(timespan)
    return timespan
  }
}

export default optimisate
