// converts problem to chromosome as an example to use in development process

function convertToChromosome(problem) {
    // console.log("converter")
    let chromosome = [
        // {   
        //     job: 1,
        //     operation: 1,
        //     machine: 1,
        //     after: 0,
        // },
    ]

    let jobsNumber = problem.jobs1.length;
    let machinesNumber = problem.machines.length;

    // console.log("chromosome converter " + jobsNumber)

    for (let i = 0; i < jobsNumber; i ++) {
        for (let o = 0; o < problem.jobs1[i].operations.length; o++) {
          chromosome.push({
            job: i,
            operation: o,
            machine: Math.floor(Math.random() * machinesNumber),
            after: 0,
          });
        }
    }

    // console.log(chromosome)
    return chromosome
}

export default convertToChromosome