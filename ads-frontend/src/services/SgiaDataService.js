import http from "../http-common";

class SgiaDataService {

  create(data) {
    return http.post("/request", data);
  }

}

export default new SgiaDataService();
