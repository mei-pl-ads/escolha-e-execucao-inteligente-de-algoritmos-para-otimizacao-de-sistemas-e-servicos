import Vue from "vue";
import VueRouter from "vue-router";
import Request from "../views/Request.vue";
import Start from "../views/Start.vue";
import Sjp from "../views/Sjp.vue";
import Gantt from "../views/Gantt.vue";
import Test from "../views/Test.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/request",
    name: "Request",
    component: Request,
  },
  {
    path: "/",
    name: "Start",
    component: Start,
  },
  {
    path: "/sjp/:problem",
    name: "Sjp",
    component: Sjp,
  },
  {
    path: "/gantt",
    name: "gantt",
    component: Gantt,
  },
  {
    path: "/test",
    name: "test",
    component: Test,
  },
  {
    path: "/result",
    name: "Result",
    // route level code-splitting
    // this generates a separate chunk (result.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "result" */ "../views/Result.vue"),
  },
];

const router = new VueRouter({
  routes
});

export default router;
