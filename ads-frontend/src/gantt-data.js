//

function getGanttData(problem, chromosome) {
  let ganttData = {
    myChartStart: "",
    myChartEnd: "",
    timeSpan: 0,
    pushOnOverlap: true,
    grid: true,
    rowHeight: 50,
    rows: [
      //   {
      //     label: "Machine #1",
      //     bars: [
      //       {
      //         myStart: "2020-03-01 12:10",
      //         myEnd: "2020-03-01 16:35",
      //         ganttBarConfig: {
      //           background: "#B29DB6",
      //           label: "Job1",
      //         },
      //       },
      //     ],
      //   },
    ],
  };

  let startDateTime = new Date(problem.startTime.join(" "));
  // console.log("Date time " + startDateTime);

  // initialise machines start time - all machines starts at startDateTime
  let machinesStarts = [];
  for (let m = 0; m < problem.machines.length; m++) {
    machinesStarts.push(startDateTime);
  }

  for (let m = 0; m < problem.machines.length; m++) {
    // 
    let operationsAssignedForMachine = chromosome.filter((item) => item.machine == m)

    // console.log("operations for machine")
    // console.table(operationsAssignedForMachine)

    let bars = [];
    for (let o = 0; o < operationsAssignedForMachine.length; o++) {
      let jobIndex = operationsAssignedForMachine[o].job
      let operationIndex = operationsAssignedForMachine[o].operation
      let operationValue = problem.jobs1[jobIndex].operations[operationIndex].value
      // time for operation process in assigned machine in miliseconds
      let leadTime = (1000 * operationValue) / problem.machines[m].value;
      // console.log(leadTime)

      let startTime = machinesStarts[m]
      let endTime = new Date(startTime.getTime() + leadTime); // convert to timestamp, add time in miliseconds and convert to Date
      machinesStarts[m] = endTime
      let jobLabel =
        problem.jobs1[jobIndex].name +
        " - " +
        problem.jobs1[jobIndex].operations[
          operationIndex
        ].name;

      bars.push({
        myStart: getDateTime(startTime),
        myEnd: getDateTime(endTime),
        ganttBarConfig: {
          background: getRandomColor(),
          label: jobLabel,
        },
      });
    }

    ganttData.rows.push({
      label: problem.machines[m].name,
      bars: bars,
    });
  }

//  console.log("Machines times ")
//  console.table(machinesStarts)

  ganttData.myChartStart = getDateTime(startDateTime)

  // console.log("Time start " + ganttData.myChartStart)
  let endChartDateTime = new Date(
    Math.max(...machinesStarts.map((e) => e.getTime()))
  )

  let timeSpan = endChartDateTime.getTime() - startDateTime.getTime()

  ganttData.timeSpan = timeSpan / 60 / 60 / 1000

  // console.log("endChartDateTime " + endChartDateTime);
  ganttData.myChartEnd = getDateTime(new Date(endChartDateTime.getTime() + 1000 * 60 * 60))

  // console.log("Time End " + ganttData.myChartEnd)

  //   console.log("Gantt chart data: ");
  //   console.log(ganttData);
  return ganttData;
}

function getRandomColor() {
  var letters = "0123456789ABCDEF";
  var color = "#";
  for (var i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}

function getDateTime(date) {
  return (
    date.getFullYear() +
    "-" +
    date.getMonth() +
    "-" +
    date.getDate() +
    " " +
    date.getHours() +
    ":" +
    date.getMinutes()
  )
}

export default getGanttData;
