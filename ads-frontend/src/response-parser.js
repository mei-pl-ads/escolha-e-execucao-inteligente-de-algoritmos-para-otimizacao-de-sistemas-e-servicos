function parseResponseToGanttData(response) {
  let parsedObj = [];
  let data = response.listGenericSolution;
  console.log("Raw responce data");
  console.log(data);
  for (let ind = 0; ind < data.length; ind++) {
    parsedObj.push(convertToGantt(data[ind]));
  }

  console.log("Parced responce data");
  console.log(parsedObj);

  return parsedObj;
}

function convertToGantt(data) {
  let ganttData = {
    myChartStart: "",
    myChartEnd: "",
    timeSpan: 0,
    consumption: 0,
    pushOnOverlap: true,
    grid: true,
    rowHeight: 50,
    rows: [
      //   {
      //     label: "Machine #1",
      //     bars: [
      //       {
      //         myStart: "2020-03-01 12:10",
      //         myEnd: "2020-03-01 16:35",
      //         ganttBarConfig: {
      //           background: "#B29DB6",
      //           label: "Job1",
      //         },
      //       },
      //     ],
      //   },
    ],
  };

  let ganttStartTime = new Date(data.timeStart);
  let ganttEndTime = new Date(data.timeEnd);
  let rows = [];
  for (let m = 0; m < data.listSchedulerResult.length; m++) {
    let row = {
      label: data.listSchedulerResult[m].machine,
      bars: [],
    };
    let operations = data.listSchedulerResult[m].listTaskResult;
    let bars = [];
    for (let o = 0; o < operations.length; o++) {
      let dateBegin = new Date(operations[o].beginOperation);
      let dateEnd = new Date(operations[o].endOperation);
      if (dateBegin < ganttStartTime) {
        ganttStartTime = dateBegin;
      }
      if (dateEnd > ganttEndTime) {
        ganttEndTime = dateEnd;
      }
      let color1 = stringToColour(operations[o].job);
      let color2 = getRandomColor();
      let background = `repeating-linear-gradient(45deg, ${color1}, ${color1} 20px, ${color2} 20px, ${color2} 30px)`;
      let bar = {
        myStart: getDateTime(dateBegin),
        myEnd: getDateTime(dateEnd),
        ganttBarConfig: {
          "text-shadow": "0px 0px 3px #000",
          background: background,
          label: operations[o].job + "-" + operations[o].operation,
        },
      };
      bars.push(bar);
    }
    row.bars = bars;
    rows.push(row);
  }
  ganttData.rows = rows;
  ganttData.timeSpan = msToTime(
    ganttEndTime.getTime() - ganttStartTime.getTime()
  );
  ganttData.myChartStart = getDateTime(
    new Date(ganttStartTime.getTime())
  );
  ganttData.myChartEnd = getDateTime(
    new Date(ganttEndTime.getTime() + 60 * 60 * 1000)
  );
  ganttData.consumption = data.consumption

  return ganttData;
}

function getRandomColor() {
  var letters = "0123456789ABCDEF";
  var color = "#";
  for (var i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}

function stringToColour(str) {
  var hash = 0;
  for (let i = 0; i < str.length; i++) {
    hash = str.charCodeAt(i) + ((hash << 6) - hash);
  }
  var colour = "#";
  for (let i = 0; i < 3; i++) {
    var value = (hash >> (i * 8)) & 0xff;
    colour += ("00" + value.toString(16)).substr(-2);
  }
  return colour;
}

function getDateTime(date) {
  return (
    date.getFullYear() +
    "-" +
    date.getMonth() +
    "-" +
    date.getDate() +
    " " +
    date.getHours() +
    ":" +
    date.getMinutes()
  );
}

function msToTime(duration) {
  var milliseconds = Math.floor((duration % 1000) / 100),
    seconds = Math.floor((duration / 1000) % 60),
    minutes = Math.floor((duration / (1000 * 60)) % 60),
    hours =
      Math.floor((duration / (1000 * 60 * 60)) % 24) +
      Math.floor(duration / (1000 * 60 * 60) / 24);

  hours = hours < 10 ? "0" + hours : hours;
  minutes = minutes < 10 ? "0" + minutes : minutes;
  seconds = seconds < 10 ? "0" + seconds : seconds;

  return hours + "h " + minutes + "m " + seconds + "s " + milliseconds + "ms";
}

export default parseResponseToGanttData;
