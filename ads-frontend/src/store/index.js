import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";
import VueAxios from "vue-axios";
import SgiaDataService from "../services/SgiaDataService";

import parseResponseToGanttData from "../response-parser"

import router from "../router";

Vue.use(Vuex);
Vue.use(VueAxios, axios);

const devMode = true;

export default new Vuex.Store({
  state: {
    // sjpData: {
    //   problemType: "Simple Job Shop",
    //   problemName: "Default problem",
    //   machines: [
    //     { name: "mch1", value: 110 },
    //     { name: "mch2", value: 215 },
    //     { name: "mch3", value: 100 },
    //   ],
    //   jobs: [
    //     { name: "j1", value: 10 },
    //     { name: "j2", value: 12 },
    //     { name: "j3", value: 20 },
    //   ],
    // },

    problem: {
      problemId: "4230980",
      problemType: "Deterministic Job Shop",
      problemName: "Default problem",
      machinesEqual: "false",
      machines: [
        { name: "machine1", value: 200, machinesNumber: 1 },
        { name: "machine2", value: 250, machinesNumber: 1 },
        { name: "machine3", value: 300, machinesNumber: 1 },
      ],
      isAllMachinesSame: "false",
      // old
      // jobs: [
      //   { name: "job1", value: 100, priority: 1, dueto: "00:00:00" },
      //   { name: "job2", value: 200, priority: 2, dueto: "00:00:00" },
      //   { name: "job3", value: 500, priority: 3, dueto: "00:00:00" },
      //   { name: "job4", value: 1000, priority: 1, dueto: "00:00:00" },
      // ],
      // jobs with operations
      jobs1: [
        {
          name: "job1",
          priority: 1,
          dueto: "00:00:00",
          operations: [
            {
              name: "operation1",
              machine: ["any"],
              order: 0,
              preemptive: "false",
              value: 500000,
              after: "none",
            },
            {
              name: "operation2",
              machine: ["any"],
              order: 1,
              preemptive: "false",
              value: 2500000,
              after: "operation2",
            },
          ],
        },
        {
          name: "job2",
          priority: 2,
          dueto: "00:00:00",
          operations: [
            {
              name: "operation1",
              machine: ["any"],
              order: 0,
              preemptive: "false",
              value: 1500000,
              after: "none",
            },
          ],
        },
        {
          name: "job3",
          priority: 2,
          dueto: "00:00:00",
          operations: [
            {
              name: "operation1",
              machine: ["any"],
              order: 0,
              preemptive: "false",
              value: 2000000,
              after: "none",
            },
            {
              name: "operation2",
              machine: ["any"],
              order: 0,
              preemptive: "false",
              value: 400000,
              after: "none",
            },
          ],
        },
      ],
      // order: [
      //   // 1-st machine index, 2-st job index
      //   [],
      // ],

      objectives: [
        { name: "Minimize the makespan", status: "true" },
        { name: "Minimize total consumption", status: "false" },
        // { name: "Objective3...", status: "false" },
      ],

      heuristic: ["first"],

      startTime: ["2021-07-13", "00:00:00"],

      aParams: {
        populationSize: 30,
        mutationProbability: 0.2,
        crossoverProbability: 0.99,
        iterations: 10,
      },
    },

    problemList: [
      {
        id: 1,
        index: "fs",
        name: "Flow Shop",
        route: "sjp/fs",
        link: "https://en.wikipedia.org/wiki/Flow_shop_scheduling",
      },
      {
        id: 2,
        index: "djs",
        name: "Deterministic Job Shop",
        route: "sjp/djs",
        link: "https://en.wikipedia.org/wiki/Job_shop_scheduling",
      },
      {
        id: 3,
        index: "os",
        name: "Open Shop",
        route: "sjp/os",
        link: "https://en.wikipedia.org/wiki/Open-shop_scheduling",
      },
    ],

    // ganttData: {
    //   myChartStart: "2020-03-01 00:00",
    //   myChartEnd: "2020-03-03 00:00",
    //   timeSpan: 0,
    //   pushOnOverlap: true,
    //   grid: true,
    //   rowHeight: 50,
    //   rows: [
    //     {
    //       label: "Machine #1",
    //       bars: [
    //         {
    //           myStart: "2020-03-01 12:10",
    //           myEnd: "2020-03-01 16:35",
    //           ganttBarConfig: {
    //             background: "#B29DB6",
    //             label: "Job1",
    //           },
    //         },
    //       ],
    //     },
    //     {
    //       label: "Machine #2",
    //       bars: [
    //         {
    //           myStart: "2020-03-02 01:00",
    //           myEnd: "2020-03-02 12:00",
    //           ganttBarConfig: {
    //             background: "#B4B4B4",
    //             label: "Job2",
    //           },
    //         },
    //         {
    //           myStart: "2020-03-02 13:00",
    //           myEnd: "2020-03-02 22:00",
    //           ganttBarConfig: {
    //             background: "#ABC3CE",
    //             label: "Job3",
    //           },
    //         },
    //       ],
    //     },
    //     {
    //       label: "Machine #3",
    //       bars: [
    //         {
    //           myStart: "2020-03-02 02:00",
    //           myEnd: "2020-03-02 13:00",
    //           ganttBarConfig: {
    //             background: "#C5AEB4",
    //             label: "Job4",
    //           },
    //         },
    //         {
    //           myStart: "2020-03-02 14:00",
    //           myEnd: "2020-03-02 23:00",
    //           ganttBarConfig: {
    //             background: "#7B8FA5",
    //             label: "Job5",
    //           },
    //         },
    //       ],
    //     },
    //     {
    //       label: "Machine #4",
    //       bars: [
    //         {
    //           myStart: "2020-03-02 00:00",
    //           myEnd: "2020-03-02 11:00",
    //           ganttBarConfig: {
    //             background: "#99B49F",
    //             label: "Job3",
    //           },
    //         },
    //         {
    //           myStart: "2020-03-01 01:00",
    //           myEnd: "2020-03-01 21:00",
    //           ganttBarConfig: {
    //             background: "#B29DB6",
    //             label: "Job3",
    //           },
    //         },
    //       ],
    //     },
    //   ],
    // },
    // mocked object
    datas: [],
    // datas: [
    //   {
    //     name: "Alpha",
    //     type: "Int",
    //     from: 0,
    //     to: 1000,
    //     index: 0,
    //   },
    //   {
    //     name: "Beta",
    //     type: "Int",
    //     from: 0,
    //     to: 1000,
    //     index: 1,
    //   },
    //   {
    //     name: "Gamma",
    //     type: "Bool",
    //     from: 0,
    //     to: 1000,
    //     index: 2,
    //   },
    //   {
    //     name: "Epsilon",
    //     type: "int",
    //     from: 0,
    //     to: 1000,
    //     index: 3,
    //   },
    // ],
    responseObj: [],
    // mocked object
    // responseObj: {
    //   "algorithmRequest": {
    //     "dataType": "Integer",
    //     "variables": [
    //       {
    //         "from": "0",
    //         "index": 0,
    //         "name": "a",
    //         "to": "10"
    //       }
    //     ],
    //     "iterations": 1,
    //     "objectives": 2,
    //     "endpoint": "http//evaluationendpoint.uri",
    //     "file": null,
    //     "typeObject": "INTEGER"
    //   },
    //   "problem": {
    //     "numberOfVariables": 1,
    //     "numberOfObjectives": 2,
    //     "numberOfConstraints": 0,
    //     "name": "IntegerProblemAlgorithm"
    //   },
    //   "result": [
    //     {
    //       "objectives": [
    //         0.0,
    //         0.0
    //       ],
    //       "numberOfVariables": 1,
    //       "numberOfObjectives": 2
    //     }
    //   ],
    //   "objetiveResult": [
    //     "0.5",
    //     "0.25",
    //     "6.0",
    //     "1.56",
    //     "10.0",
    //     "8.0",
    //     "0.876",
    //     "7.85"
    //   ],
    //   "variablesResult": [
    //     "77",
    //     "8",
    //     "9",
    //     "11",
    //     "53",
    //     "5",
    //     "4",
    //   ]
    // },

    // dataTypes: ["Boolean", "Double", "Integer"],

    // dataType: "",
    // endpoint: "",
    // iterations: 1,
    // objectives: 1,
    avaitStatus: false,
    // isJarUploaded: false,
    errorStatus: false,
    errorMessage: "Something went wrong! ",
  },

  getters: {
    getDatas: (state) => {
      // let objToSend = {
      //   dataType: state.dataType,
      //   iterations: state.iterations,
      //   objectives: state.objectives,
      //   endpoint: state.endpoint,
      //   variables: state.datas,
      // };
      let objToSend = state.problem;
      return objToSend;
    },
  },

  mutations: {
    setProblemName(state, value) {
      state.problem.problemName = value;
      if (devMode) console.log("problemName updated: " + value);
    },

    setProblemType(state, value) {
      state.problem.problemType = value;
    },

    setDataType(state, value) {
      state.dataType = value;
      if (devMode) console.log("DataType updated: " + value);
    },

    setStartTime(state, value) {
      state.problem.startTime[0] = value;
      if (devMode) console.log("startTime updated: " + value);
    },
    setStartDate(state, value) {
      state.problem.startTime[1] = value;
      if (devMode) console.log("startDate updated: " + value);
    },

    setIsAllMachinesSame(state, value) {
      state.problem.isAllMachinesSame = value;
    },

    addVar(state) {
      state.datas.push({
        name: "",
        from: "0",
        to: "1",
        index: state.datas.length,
      });
      if (devMode) console.log("New element added");
    },

    removeVar(state, index) {
      state.datas.splice(index, 1);
      if (devMode) console.log("Element " + index + " deleted");
    },

    updateEndpoint(state, value) {
      state.endpoint = value;
      if (devMode) console.log("Endpoint Uri updated: " + value);
    },

    updateIterations(state, value) {
      state.iterations = value;
      if (devMode) console.log("Iterations updated: " + value);
    },

    updateObjectives(state, value) {
      state.objectives = value;
      if (devMode) console.log("Objectives updated: " + value);
    },

    setPayload(state, payload) {
      state.responseObj = parseResponseToGanttData(payload);
    },

    setAvaitStatus(state, status) {
      state.avaitStatus = status;
    },

    setJarUploaded(state, value) {
      state.isJarUploaded = value;
      if (devMode) console.log("isJarUploaded updated: " + value);
    },

    setErrorStatus(state, status) {
      state.errorStatus = status;
      if (devMode) console.log("Error Status updated: " + status);
    },

    setErrorMessage(state, message) {
      state.errorMessage = message;
      if (devMode) console.log("Error Message updated: " + message);
    },

    clearErrorMessage(state) {
      state.errorMessage = "";
      if (devMode) console.log("Error Message cleared: ");
    },

    // todo delete before deploying
    setChromosome(state, chromosome) {
      state.chromosome = chromosome;
      if (devMode) console.log("Chromosome committed");
    },

    setGanttData(state, value) {
      state.ganttData = value;
      if (devMode) console.log("Gantt Data committed");
    },
  },

  actions: {
    submitData({ commit, getters }) {
      if (devMode) console.log("submitData action called");

      commit("setAvaitStatus", true);
      SgiaDataService.create(getters.getDatas)
        .then((response) => response.data)
        .then((payload) => {
          if (devMode) console.log(payload);
          commit("setPayload", payload);
          commit("setAvaitStatus", false);
          router.push({ path: "/gantt" });
        })
        .catch((e) => {
          if (devMode) console.log("Request Error - " + e.message);
          commit("setErrorStatus", true);
          commit(
            "setErrorMessage",
            "Input data is incorrect! Check inputs and try again."
          );
          // commit("setPayload", {
          //   error: {
          //     name: "Unexpected error",
          //     value: e.message,
          //   },
          // });

          commit("setAvaitStatus", false);
        });
    },
  },
  modules: {},
});
