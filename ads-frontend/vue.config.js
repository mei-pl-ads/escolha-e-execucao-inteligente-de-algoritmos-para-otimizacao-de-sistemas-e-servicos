module.exports = {
  css: {
    sourceMap: true,
    loaderOptions: {
      sass: {
        prependData: `
            @import "@/assets/scss/main.scss";
            `,
      },
    },
  },
  publicPath: process.env.NODE_ENV === "production" ? "./" : "/",
  devServer: {
    port: 8081,
  },
};